package org.streetjournal.mobileapi.xml.xmlrequest.issue;

import org.streetjournal.mobileapi.xml.AbstractRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.LocationData;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Данные для запроса на вход
 */
@XmlRootElement(name="Request")
@XmlType(name = "IssueCreateRequestType")
public class IssueCreateRequest extends AbstractRequest {

  private String token;

  private String region;

  private String title;

  private String description;

  private String address;

  private LocationData location;

  private String category;

  private Boolean anonymous;

  public IssueCreateRequest() {
  }

  @XmlAttribute(name = "token")
  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @XmlElement(name = "Region")
  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  @XmlElement(name = "Title")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @XmlElement(name = "Description")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @XmlElement(name = "Address")
  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @XmlElement(name = "Location")
  public LocationData getLocation() {
    return location;
  }

  public void setLocation(LocationData location) {
    this.location = location;
  }

  @XmlElement(name="Category")
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  @XmlElement(name="Anonymous")
  public Boolean getAnonymous() {
    return anonymous;
  }

  public void setAnonymous(Boolean anonymous) {
    this.anonymous = anonymous;
  }

}

package org.streetjournal.mobileapi.api.system;

import biz.gelicon.sjua.model.UserModel;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.system.RecoverPasswordRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.system.RecoverPasswordResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 */
@javax.ws.rs.Path("recover_password")
public class RecoverPasswordHandler {

    private static final Logger logger = LoggerFactory.getLogger(RecoverPasswordHandler.class);

    @Inject
    EntityManager em;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public AbstractResponse execute(RecoverPasswordRequest request) {

        UserModel user = checkLogin(request.getEmail());

        if (user == null) {
            return new RecoverPasswordResponse(RecoverPasswordResponse.Status.NOT_FOUND);
        }

        Client c = Client.create();
        WebResource r = c.resource("http://opencity.in.ua/register_json");

        JSONObject requestData = new JSONObject();

        try {
            requestData
                    .put("action", "restorepassword")
                    .put("email", request.getEmail());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        String bean = r
                .entity(requestData)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .post(String.class);

        JSONObject resultBean;

        try {
            resultBean = new JSONObject(bean);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        boolean success;

        try {
            success = resultBean.getBoolean("success");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        logger.info("Result=[{}]", bean.toString());

        if (success) {
            return new RecoverPasswordResponse(RecoverPasswordResponse.Status.OK);
        }

        throw new IllegalStateException();

    }


    private UserModel checkLogin(String login) {

        UserModel user;

        try {

            user =
                    em.createQuery("SELECT e FROM User e WHERE LOWER(e.email)=:name", UserModel.class)
                            .setParameter(
                                    "name", login.toLowerCase()).getSingleResult();

        } catch (NoResultException e) {
            return null;
        }

        return user;

    }

}
package biz.gelicon.sjua.model;

import javax.persistence.*;

/**
 * Статус проблемы
 */
@Entity(name = "IssueStatus")
@Table(name = "issuestatus")
//@SequenceGenerator(name="IssueStatusIdGen", sequenceName = "issuestatus_id_gen")
public class IssueStatusModel {

    /**
     * Открыта
     */
    public static final int STATUS_OPEN = 1;

    /**
     * Принята
     */
    public static final int STATUS_ACCEPTED = 2;

    /**
     * Закрыта
     */
    public static final int STATUS_CLOSED = 3;

    /**
     * Архивирована
     */
    public static final int STATUS_ARCHIVE = 4;

    /**
     * Опубликовано
     */
    public static final int STATUS_PUBLIC_TYPE_2 = 5;

    /**
     * Опубликовано
     */
    public static final int STATUS_PUBLIC_TYPE_3 = 6;

    /**
     * Опубликовано
     */
    public static final int STATUS_PUBLIC_TYPE_4 = 7;

    private Integer id;

    private String name;

    private LayerTypeModel type;

    @Id
//  @GeneratedValue(generator="IssueStatusIdGen")
    @Column(name = "issuestatus_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "issuestatus_name", length = 20, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

  @ManyToOne
  @JoinColumn(name = "layertype_id", nullable = false)
  public LayerTypeModel getType() {
    return type;
  }

  public void setType(LayerTypeModel type) {
    this.type = type;
  }

}

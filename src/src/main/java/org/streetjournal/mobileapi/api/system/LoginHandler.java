package org.streetjournal.mobileapi.api.system;

import biz.gelicon.server.service.PasswordHash;
import biz.gelicon.server.service.TicketControl;
import biz.gelicon.sjua.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.issue.IssueDataRequest;
import org.streetjournal.mobileapi.xml.xmlrequest.system.LoginRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.system.LoginErrorResponse;
import org.streetjournal.mobileapi.xml.xmlresponse.system.LoginResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 */
@javax.ws.rs.Path("login")
public class LoginHandler {

  private static final Logger logger = LoggerFactory.getLogger(LoginHandler.class);

  @Inject
  EntityManager em;

  @Inject
  TicketControl ticketControl;

  @Inject
  PasswordHash passwordHash;

  private LoginRequest request;

  @POST
  @Consumes(MediaType.APPLICATION_XML)
  @Produces(MediaType.APPLICATION_XML)
  public AbstractResponse execute(LoginRequest request) {

    this.request = request;

    UserModel user = checkLogin();

    if (user != null) {
      ticketControl.setTicket(System.currentTimeMillis(), user.getId());
      return new LoginResponse(ticketControl.getHexTicket(), user.getId().longValue(), formatName(user), "ok", 0);
    } else {
      return new LoginErrorResponse("bad_login", "Bad login or password");
    }

  }

  private UserModel checkLogin() {

    UserModel user;

    try {

      user =
              em.createQuery("SELECT e FROM User e WHERE LOWER(e.email)=:name", UserModel.class)
                      .setParameter(
                              "name", request.getLogin().toLowerCase()).getSingleResult();

    } catch (NoResultException e) {
      return null;
    }

    if (!passwordHash.checkPassword(request.getPassword(), user.getPassword())) {

      logger.info(String.format("Bad password for user=[%s]", request.getLogin()));

      return null;

    }

    return user;

  }

  // @TODO Вынести formatName + appendWord в хелпер
  private static String formatName(UserModel user) {

    StringBuilder sb = new StringBuilder();

    appendWord(sb, user.getName());
    appendWord(sb, user.getSurname());

    return sb.toString();

  }

  private static void appendWord(StringBuilder stringBuilder, String value) {

    if (value != null && value.length() > 0) {
      if (stringBuilder.length() > 0) {
        stringBuilder.append(' ');
      }
      stringBuilder.append(value);
    }

  }

}
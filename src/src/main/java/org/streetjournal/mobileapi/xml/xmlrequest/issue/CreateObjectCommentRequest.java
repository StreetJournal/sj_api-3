package org.streetjournal.mobileapi.xml.xmlrequest.issue;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Данные для запроса на вход
 */
@XmlRootElement(name = "Request")
@XmlType(name = "CreateObjectComment")
public class CreateObjectCommentRequest extends AbstractRequest {

    private String token;

    private Integer objectId;

    private Integer replyToId;

    private String text;

    private Boolean anonymous;

    public CreateObjectCommentRequest() {
    }

    @XmlAttribute(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlElement(name = "ObjectId")
    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    @XmlElement(name = "ReplyTo")
    public Integer getReplyToId() {
        return replyToId;
    }

    public void setReplyToId(Integer replyToId) {
        this.replyToId = replyToId;
    }

    @XmlElement(name = "Text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @XmlElement(name = "Anonymous")
    public Boolean getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Boolean anonymous) {
        this.anonymous = anonymous;
    }

}

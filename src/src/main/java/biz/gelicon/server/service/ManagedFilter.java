package biz.gelicon.server.service;

import biz.gelicon.server.api.ThreadManagedScope;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

@Singleton
public class ManagedFilter implements Filter {

  private final ThreadManagedScope scope;

  @Inject
  public ManagedFilter(ThreadManagedScope scope) {
    this.scope = scope;
  }

  @Override
  public void init(FilterConfig config) throws ServletException {
  }

  @Override
  public void destroy() {
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filter)
          throws IOException, ServletException {

    scope.enter();

    try {
      filter.doFilter(request, response);
    } finally {
      scope.exit();
    }

  }

}

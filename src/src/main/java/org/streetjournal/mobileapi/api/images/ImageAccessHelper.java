package org.streetjournal.mobileapi.api.images;

import biz.gelicon.sjua.model.IssueCommentImageModel;
import biz.gelicon.sjua.model.IssueCommentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.service.UrlHelper;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.ImageData;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 *
 */
public class ImageAccessHelper {

    private static final Logger logger = LoggerFactory.getLogger(ImageAccessHelper.class);

    private final static int PREVIEW_IMAGE_SIZE = 96;

    @Inject
    EntityManager em;
    @Inject
    UrlHelper urlHelper;

    public byte[] getImageData(Integer id) {
        return getImageData(id, false);
    }

    public byte[] getImageData(Integer id, boolean preview) {
        byte[] data = (byte[]) em.createQuery("SELECT e.data from IssueCommentImage e where e.id = :id")
                .setParameter("id", id).getSingleResult();
        if (preview) {
            data = resize(data, PREVIEW_IMAGE_SIZE);
        }
        return data;
    }

    public List<ImageData> getPreviewImages(IssueCommentModel issueComment) {

        List<ImageData> result = new ArrayList<>();

        int order = 0;

        for (IssueCommentImageModel item : em.createQuery(
            "SELECT e FROM IssueCommentImage e WHERE e.comment=:comment ORDER BY e.number",
            IssueCommentImageModel.class)
            .setParameter("comment", issueComment).getResultList()) {

            try {
                result.add(createImageData(item.getId(), order));
                order++;
            } catch (Exception e) {
                logger.error("Image conversion error", e);
            }

        }

        return result;

    }

    public Map<IssueCommentModel, List<ImageData>> getPreviewImages(Collection<IssueCommentModel> issueComments) {

        Map<IssueCommentModel, List<ImageData>> result = new HashMap<>();

        if (issueComments != null && issueComments.size() != 0) {

            int order = 0;

            for (IssueCommentImageModel item : em.createQuery(
                "SELECT e FROM IssueCommentImage e WHERE e.comment in :comments ORDER BY e.number",
                IssueCommentImageModel.class)
                .setParameter("comments", issueComments).getResultList()) {

                try {
                    List<ImageData> list = result.get(item.getComment());

                    if (list == null) {
                        result.put(item.getComment(), list = new ArrayList<>());
                    }

                    list.add(createImageData(item.getId(), order));

                    order++;
                } catch (Exception e) {
                    logger.error("Image conversion error", e);
                }

            }

        }

        return result;
    }

    private byte[] resize(byte[] data, int targetSize) {

        BufferedImage image;

        try {
            image = ImageIO.read(new ByteArrayInputStream(data));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        int sourceSize = Math.min(image.getWidth(), image.getHeight());

        image = image.getSubimage(0, 0, sourceSize, sourceSize);

        int type = image.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : image.getType();

        Image scaledImage = image.getScaledInstance(targetSize, targetSize, Image.SCALE_FAST);

        image = new BufferedImage(targetSize, targetSize, type);

        image.getGraphics().drawImage(scaledImage, 0, 0, null);

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, "jpeg", output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return output.toByteArray();

    }

    public ImageData createImageData(Integer id, Integer order) {
        return new ImageData()
                .setId(id)
                .setOrder(order)
                .setUrl(urlHelper.getImageUrl(id, false))
                .setPreviewUrl(urlHelper.getImageUrl(id, true));
    }

}

package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Response")
public class IssueDataResponse extends AbstractResponse {

  private List<IssueData> issues;

  public IssueDataResponse() {
  }

  public IssueDataResponse(List<IssueData> issues) {
    super(0, "Data found");
    this.issues = issues;
  }

  @XmlElementRef
  public List<IssueData> getIssues() {
    return issues;
  }

}

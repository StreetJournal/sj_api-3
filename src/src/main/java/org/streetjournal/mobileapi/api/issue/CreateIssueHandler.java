package org.streetjournal.mobileapi.api.issue;

import biz.gelicon.server.service.TicketControl;
import biz.gelicon.sjua.model.IssueCommentModel;
import biz.gelicon.sjua.model.IssueCommentTypeModel;
import biz.gelicon.sjua.model.IssueLayerBindModel;
import biz.gelicon.sjua.model.IssueModel;
import biz.gelicon.sjua.model.IssueStatusModel;
import biz.gelicon.sjua.model.LayerModel;
import biz.gelicon.sjua.model.MapTypeModel;
import biz.gelicon.sjua.model.ToponymModel;
import biz.gelicon.sjua.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.service.geo.RegionGeocodeHelper;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.issue.IssueCreateRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.IssueCreateResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 */
@javax.ws.rs.Path("create_issue")
public class CreateIssueHandler {

    private static final Logger logger = LoggerFactory.getLogger(CreateIssueHandler.class);

    @Inject
    EntityManager em;

    @Inject
    TicketControl ticketControl;

    @Inject
    RegionGeocodeHelper regionGeocodeHelper;

    private IssueCreateRequest request;

    private Integer issueId;

    private Integer userId;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public AbstractResponse execute(IssueCreateRequest request) {

        this.request = request;

        authentificate();

        em.getTransaction().begin();

        createIssue();

        em.getTransaction().commit();

        return new IssueCreateResponse(issueId);

    }

    private void authentificate() {
        ticketControl.setHexTicket(request.getToken());
        userId = ticketControl.getIntValue();
    }

    private void createIssue() {

        Date now = new Date();

        IssueModel issue = new IssueModel();

        issue.setStatus(em.getReference(IssueStatusModel.class, IssueStatusModel.STATUS_OPEN));

        if (request.getLocation() != null) {
            Integer lat = request.getLocation().getLat();
            Integer lng = request.getLocation().getLng();

            if (lat != null && lng != null) {
                issue.setLalitude(lat * 1E-6);
                issue.setLongitude(lng * 1E-6);
                issue.setToponym(findRegion(regionGeocodeHelper.getRegion(lat, lng)));
            } else {
                issue.setToponym(findRegion(request.getRegion()));
            }
        } else {
            issue.setToponym(findRegion(request.getRegion()));
        }

        issue.setName(request.getTitle());
        issue.setAddress(request.getAddress());

        issue.setMapType(em.getReference(MapTypeModel.class, 3));

        issue.setModerationDate(new GregorianCalendar(1900, 0, 1).getTime());
        issue.setBeginDate(new GregorianCalendar(1900, 0, 1).getTime());
        issue.setEndDate(new GregorianCalendar(2099, 11, 31).getTime());

        issue.setMailMode(IssueModel.CANEMAIL_UNDEFINED);

        issue.setViewCount(0);

        em.persist(issue);

        IssueCommentModel issueComment = new IssueCommentModel();

        issueComment.setIssue(issue);
        issueComment.setUser(em.getReference(UserModel.class, userId));
        issueComment.setType(em.getReference(IssueCommentTypeModel.class, IssueCommentTypeModel.TYPE_STATUS));
        issueComment.setIssueStatus(em.getReference(IssueStatusModel.class, IssueStatusModel.STATUS_OPEN));
        issueComment.setAnonymous(request.getAnonymous() != null && request.getAnonymous());
        issueComment.setDeleteStatus(IssueCommentModel.STATUS_NORMAL);
        issueComment.setText(request.getDescription());

        issueComment.setCreationDate(now);
        issueComment.setModificationDate(now);

        em.persist(issueComment);

        IssueLayerBindModel layerBind = new IssueLayerBindModel();

        layerBind.setMain(true);
        layerBind.setIssue(issue);
        layerBind.setLayer(
            em.createQuery("SELECT e FROM Layer e WHERE e.code=:code", LayerModel.class)
                .setParameter("code", request.getCategory())
                .getSingleResult()
        );

        em.persist(layerBind);

        issueId = issue.getId();

    }

    private ToponymModel findRegion(String region) {

        if(region == null || region.length()==0) {
            return findRegion("UA");
        }

        String country = region.substring(0, 2);
        String code = region.substring(2);

        return em.createQuery("SELECT e FROM Toponym e WHERE e.country.code=:country AND e.code=:code", ToponymModel.class)
            .setParameter("country", country)
            .setParameter("code", code)
            .getSingleResult();

    }

}

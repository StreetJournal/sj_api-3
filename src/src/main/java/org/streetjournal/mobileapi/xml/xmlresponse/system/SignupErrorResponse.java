package org.streetjournal.mobileapi.xml.xmlresponse.system;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 */
@XmlRootElement(name = "Response")
public class SignupErrorResponse extends AbstractResponse {

  public SignupErrorResponse() {
    super(1, "Signup error");
  }

}

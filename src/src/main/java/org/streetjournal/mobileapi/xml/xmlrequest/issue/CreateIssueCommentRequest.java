package org.streetjournal.mobileapi.xml.xmlrequest.issue;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Данные для запроса на вход
 */
@XmlRootElement(name = "Request")
@XmlType(name = "CreateIssueComment")
public class CreateIssueCommentRequest extends AbstractRequest {

    private String token;

    private Integer issueId;

    private Integer replyToId;

    private String text;

    private Boolean anonymous;

    public CreateIssueCommentRequest() {
    }

    @XmlAttribute(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlElement(name = "IssueId")
    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

    @XmlElement(name = "ReplyTo")
    public Integer getReplyToId() {
        return replyToId;
    }

    public void setReplyToId(Integer replyToId) {
        this.replyToId = replyToId;
    }

    @XmlElement(name = "Text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @XmlElement(name = "Anonymous")
    public Boolean getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Boolean anonymous) {
        this.anonymous = anonymous;
    }

}

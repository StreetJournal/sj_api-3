package org.streetjournal.mobileapi.service;

import biz.gelicon.server.api.Managed;
import biz.gelicon.sjua.model.ToponymModel;
import org.streetjournal.mobileapi.config.Configuration;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.*;

/**
 *
 */
@Managed
public class ToponymTreeHelper {

    private final List<ToponymModel> emptyList = Collections.unmodifiableList(new ArrayList<ToponymModel>(0));

    private EntityManager em;

    private Map<ToponymModel, List<ToponymModel>> tree;

    private List<ToponymModel> roots;

    @Inject
    private Configuration configuration;

    @Inject
    public ToponymTreeHelper(EntityManager em) {
        this.em = em;
    }

    /**
     * Построение списка регионов ниже и выше от заданного корневого
     *
     * @param root
     * @return
     */
    public List<ToponymModel> getToponymTreeSection(ToponymModel root) {

        List<ToponymModel> result = getToponymTree(root);

        for (; ; ) {
            root = root.getParent();
            if (root == null || root.getParent() == root) {
                break;
            }
            result.add(root);
        }

        return result;

    }

    /**
     * Построение списка регионов ниже от заданного корневого
     *
     * @param root
     * @return
     */
    public List<ToponymModel> getToponymTree(ToponymModel root) {

        cacheToponyms();

        List<ToponymModel> result = new ArrayList<>();

        addChilds(result, root);

        return result;

    }

    public List<ToponymModel> getRoots() {
        cacheToponyms();
        return Collections.unmodifiableList(roots);
    }

    public List<ToponymModel> getChilds(ToponymModel root) {
        cacheToponyms();
        List<ToponymModel> childs = tree.get(root);
        return childs != null ? Collections.unmodifiableList(childs) : emptyList;
    }

    private void addChilds(List<ToponymModel> result, ToponymModel root) {

        result.add(root);

        List<ToponymModel> childs = tree.get(root);

        if (childs != null) {

            for (ToponymModel item : childs) {
                addChilds(result, item);
            }

        }

    }

    private void cacheToponyms() {

        if (tree == null) {

            roots = new ArrayList<>();

            tree = new HashMap<>();

            String region = configuration.regionCode;
            TypedQuery<ToponymModel> query;
            if(region != null) {
                String countryCode = region.substring(0,2);
                String regionCode = region.substring(2);
                query = em.createQuery(
                        "SELECT e FROM Toponym e WHERE e.country.code = :country AND e.code LIKE :region ORDER BY e.code",
                        ToponymModel.class)
                        .setParameter("country", countryCode)
                        .setParameter("region", regionCode + "%");
            } else {
                query = em.createQuery("SELECT e FROM Toponym e WHERE e.country IS NOT NULL ORDER BY e.code", ToponymModel.class);
            }

            for (ToponymModel item : query.getResultList()) {
                ToponymModel parent = (region != null && item.getCode().equals(region.substring(2)) ? null : item.getParent());
                if (item.getParent() != null && item.getType() == 0 && item.getParent() != item) {
                    continue;
                }
                if (parent != null && item.getParent().getType() == 0 && item.getParent().getLevel() != 1) {
                    parent = item.getParent().getParent();
                }
                if (parent != null) {
                    List<ToponymModel> parentChilds = tree.get(parent);
                    if (parentChilds == null) {
                        tree.put(parent, parentChilds = new ArrayList<ToponymModel>());
                    }
                    parentChilds.add(item);
                } else {
                    tree.put(item, new ArrayList<ToponymModel>());
                    roots.add(item);
                }

            }

        }

    }

}

package localhost;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.streetjournal.mobileapi.service.geo.GeoRegions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Created with IntelliJ IDEA.
 * User: belan_000
 * Date: 09.10.13
 * Time: 19:40
 * To change this template use File | Settings | File Templates.
 */
public class TestGeoRequest {

//        http://opencity.in.ua/geo/regions/search?lat=50391065&lng=30518290

    public static void main(String[] args) {

        Client c = Client.create();
        WebResource r = c.resource("http://opencity.in.ua/geo/regions/search");

        GeoRegions bean = r
            .queryParam("lat", "50391065")
            .queryParam("lng", "30518290")
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
            .accept(MediaType.APPLICATION_JSON_TYPE)
            .get(GeoRegions.class);

        System.out.println("Done");
    }

}

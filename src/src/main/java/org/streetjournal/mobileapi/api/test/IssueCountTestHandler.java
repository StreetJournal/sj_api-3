package org.streetjournal.mobileapi.api.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Тестовый обработчик, возвращающий количество проблем в системе
 */

@Path("issue_count_test")
public class IssueCountTestHandler {

  private static final Logger logger = LoggerFactory.getLogger(IssueCountTestHandler.class);

  @Inject
  EntityManager em;

  @GET
  public String execute() {

    Long value = em.createQuery("SELECT count(*) FROM Issue e", Long.class).getSingleResult();

    return "Issue count = " + value;

  }

}

package org.streetjournal.mobileapi.config;

import biz.gelicon.server.config.DatabaseConfig;
import biz.gelicon.server.config.TicketConfig;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

//TODO: сделать геттеры, перевести все на геттеры
@XmlRootElement(name = "Config")
public class Configuration {

    @XmlElementRef
    public DatabaseConfig database;

    @XmlElementRef
    public TicketConfig security;

    @XmlElementRef
    public ShapeFileConfig shapeFileConfig;

    @XmlElement(name = "Host")
    public String host;

    @XmlElement(name = "CategoryGroup")
    public Set<String> categoryGroupCodes;

    @XmlElement(name = "LayerType")
    public Set<Integer> layerTypes;

    @XmlElement(name = "Region")
    public String regionCode;

}

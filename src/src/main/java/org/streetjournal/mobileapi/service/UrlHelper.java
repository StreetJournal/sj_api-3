package org.streetjournal.mobileapi.service;

import org.streetjournal.mobileapi.api.issue.GetIssueImageHandler;
import org.streetjournal.mobileapi.config.Configuration;

import javax.inject.Inject;
import javax.ws.rs.Path;

public class UrlHelper {

    @Inject
    private Configuration configuration;

    private final static String IMAGE_GET_URL = GetIssueImageHandler.class.getAnnotation(Path.class).value();

    public String getImageUrl(Integer id) {
        return getImageUrl(id, false);
    }

    public String getImageUrl(Integer id, boolean preview) {
        return getBase(false) + IMAGE_GET_URL + "/" + id + (preview ? "?preview=true" : "");
    }

    private String getBase(boolean https) {
        return "http" + (https ? "s" : "") + "://" + configuration.host
                + (configuration.host.endsWith("/") ? "" : "/");
    }

    public UrlHelper setConfiguration(Configuration configuration) {
        this.configuration = configuration;
        return this;
    }

}

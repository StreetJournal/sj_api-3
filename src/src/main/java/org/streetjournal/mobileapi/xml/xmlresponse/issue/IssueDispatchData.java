package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.TimestampAdapter;
import org.streetjournal.mobileapi.xml.xmlresponse.organization.OrganizationData;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import java.util.Date;

/**
 *
 */
@XmlRootElement(name = "Dispatch")
public class IssueDispatchData {

	private Integer id;
    private Date date;

    private OrganizationData organization;

    @XmlJavaTypeAdapter(TimestampAdapter.class)
    @XmlElement(name = "Date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @XmlElementRef
    public OrganizationData getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationData organization) {
        this.organization = organization;
    }
    
    public void setId(Integer id) {
    	this.id = id;
    }
    
    @XmlAttribute(name = "id")
    public Integer getId() {
    	return this.id;
    }

}

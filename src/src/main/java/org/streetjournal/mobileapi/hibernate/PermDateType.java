package org.streetjournal.mobileapi.hibernate;

import org.hibernate.type.descriptor.ValueBinder;
import org.hibernate.type.descriptor.ValueExtractor;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.JavaTypeDescriptor;
import org.hibernate.type.descriptor.sql.BasicBinder;
import org.hibernate.type.descriptor.sql.BasicExtractor;
import org.hibernate.type.descriptor.sql.TimestampTypeDescriptor;

import java.sql.*;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Костыль, написанный из-за того, что какие-то ебланы решили
 * хранить в базе время местное, а не UTC. Т.е. вместо 10:00 в базе хранится 16:00
 * Временное исправление этого феноменального пиздеца - это написание своего типа,
 * который будет производить конвертацию времени прозрачно для приложения.
 * От этой хуйни надо избавляться и делать по уму. Извините.
 */
public class PermDateType extends TimestampTypeDescriptor {
    public static final PermDateType INSTANCE = new PermDateType();

    private static final TimeZone PERM = TimeZone.getTimeZone("Etc/GMT+6");

    public <X> ValueBinder<X> getBinder(final JavaTypeDescriptor<X> javaTypeDescriptor) {
        return new BasicBinder<X>( javaTypeDescriptor, this ) {
            @Override
            protected void doBind(PreparedStatement st, X value, int index, WrapperOptions options) throws SQLException {
                st.setTimestamp( index, javaTypeDescriptor.unwrap( value, Timestamp.class, options ), Calendar.getInstance(PERM) );
            }
        };
    }

    public <X> ValueExtractor<X> getExtractor(final JavaTypeDescriptor<X> javaTypeDescriptor) {
        return new BasicExtractor<X>( javaTypeDescriptor, this ) {
            @Override
            protected X doExtract(ResultSet rs, String name, WrapperOptions options) throws SQLException {
                return javaTypeDescriptor.wrap(rs.getTimestamp(name), options);
            }

            protected X doExtract(CallableStatement statement, int index, WrapperOptions options) throws SQLException {
                return javaTypeDescriptor.wrap(statement.getTimestamp(index), options);
            }

            protected X doExtract(CallableStatement statement, String name, WrapperOptions options) throws SQLException {
                return javaTypeDescriptor.wrap(statement.getTimestamp(name), options);
            }
        };
    }

}

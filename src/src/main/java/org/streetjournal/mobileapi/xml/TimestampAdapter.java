package org.streetjournal.mobileapi.xml;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimestampAdapter extends XmlAdapter<String, Date> {

    private static final ThreadLocal<SimpleDateFormat> formatter = new ThreadLocal<SimpleDateFormat>(){
        @Override
        protected SimpleDateFormat initialValue() {
            SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmss");
            sd.setTimeZone(TimeZone.getTimeZone("UTC"));
            return sd;
        }
    };

    @Override
    public Date unmarshal(String value) throws Exception {
        return formatter.get().parse(value);
    }

    @Override
    public String marshal(Date value) throws Exception {
        return formatter.get().format(value);
    }

}
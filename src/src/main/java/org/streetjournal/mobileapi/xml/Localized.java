package org.streetjournal.mobileapi.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 */
@XmlRootElement(name = "Localized")
public class Localized {

    private String locale;

    private String value;

    public Localized() {
    }

    public Localized(String locale, String value) {
        this.locale = locale;
        this.value = value;
    }

    @XmlAttribute(name = "locale")
    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @XmlValue
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}

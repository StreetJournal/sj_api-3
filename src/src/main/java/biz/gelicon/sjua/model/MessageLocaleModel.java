package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Локаль
 */
@Entity(name = "MessageLocale")
@Table(name = "messagelocale")
@SequenceGenerator(name="MessageLocaleIdGen", sequenceName = "messagelocale_id_gen")
public class MessageLocaleModel {

  private Integer id;

  private String code;

  @Id
  @GeneratedValue(generator="MessageLocaleIdGen")
  @Column(name = "messagelocale_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "messagelocale_code", nullable = false, length = 50)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

}

package org.streetjournal.mobileapi.xml.xmlresponse.object;

import org.streetjournal.mobileapi.xml.TimestampAdapter;
import org.streetjournal.mobileapi.xml.xmlresponse.LocationData;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.AuthorData;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.ImageData;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Object")
public class ObjectData {

    private Integer id;

    private Long serial;

    private String name;

    private Date creationDate;

    private Date updateDate;

    private String address;

    private String description;

    private AuthorData author;

    private LocationData location;

    private Integer votes;

    private Integer events;

    private List<ImageData> images;

    public ObjectData() {
    }

    @XmlAttribute(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlAttribute(name = "serial")
    public Long getSerial() {
        return serial;
    }

    public void setSerial(Long serial) {
        this.serial = serial;
    }

    @XmlElement(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlJavaTypeAdapter(TimestampAdapter.class)
    @XmlElement(name = "Created")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @XmlJavaTypeAdapter(TimestampAdapter.class)
    @XmlElement(name = "Updated")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @XmlElement(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @XmlElement(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement(name = "Author")
    public AuthorData getAuthor() {
        return author;
    }

    public void setAuthor(AuthorData author) {
        this.author = author;
    }

    @XmlElement(name = "Location")
    public LocationData getLocation() {
        return location;
    }

    public void setLocation(LocationData location) {
        this.location = location;
    }

    @XmlElement(name = "Votes")
    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    @XmlElement(name = "Events")
    public Integer getEvents() {
        return events;
    }

    public void setEvents(Integer events) {
        this.events = events;
    }

    @XmlElementRef
    public List<ImageData> getImages() {
        return images;
    }

    public void setImages(List<ImageData> images) {
        this.images = images;
    }

}

package org.streetjournal.mobileapi.xml;

/**
 *
 */
public class Bound {

    public int east;

    public int west;

    public int south;

    public int north;

    public Bound() {
    }

    public Bound(int south, int west, int north, int east) {
        this.east = east;
        this.west = west;
        this.south = south;
        this.north = north;
    }

}

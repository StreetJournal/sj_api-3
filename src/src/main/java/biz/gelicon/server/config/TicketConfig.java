package biz.gelicon.server.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Блок конфигурации параметров безопасности
 */
@XmlRootElement(name = "Security")
public class TicketConfig {

  /**
   * Строковый ключ, используемый для генерации ключа и создания идентификационных ярлыков
   */
  @XmlElement(name = "Secret")
  public String secret;

  /**
   * Время жизни идентификационных ярлыков в секундах
   */
  @XmlElement(name = "TimeToLive")
  public Integer timeToLive;

}

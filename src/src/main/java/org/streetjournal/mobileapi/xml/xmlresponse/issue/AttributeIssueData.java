package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import biz.gelicon.sjua.model.AttributeType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Attribute")
public class AttributeIssueData {
    private String attribute;
    private String value;
    private AttributeType type;

    @XmlElement(name = "Name")
    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    @XmlElement(name = "Value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @XmlElement(name = "Type")
    public AttributeType getType() {
        return type;
    }

    public void setType(AttributeType type) {
        this.type = type;
    }
}

package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Список идентификаторов проблем
 */
@XmlRootElement(name = "Response")
public class IssueListResponse extends AbstractResponse {

  private List<Integer> issues;

  public IssueListResponse() {
  }

  public IssueListResponse(List<Integer> issues) {
    super(0, "Found " + issues.size() + " issues(s).");
    this.issues = issues;
  }

  @XmlElement(name = "IssueId")
  public List<Integer> getIssues() {
    return issues;
  }

}

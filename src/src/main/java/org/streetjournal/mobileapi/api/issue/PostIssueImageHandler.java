package org.streetjournal.mobileapi.api.issue;

import biz.gelicon.server.service.TicketControl;
import biz.gelicon.sjua.model.IssueCommentImageModel;
import biz.gelicon.sjua.model.IssueCommentModel;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 *
 */
@Path("post_issue_image")
public class PostIssueImageHandler {

    @Inject
    EntityManager em;

    @Inject
    TicketControl ticketControl;

    private Integer userId;

    private IssueCommentModel rootComment;

    @POST
    @Path("{issue}/{token}")
    @Consumes("image/jpeg")
    public void post(@PathParam("token") String token, @PathParam("issue") Integer issueId, byte[] data) {
//    public void post(InputStream inputStream) {

//        String token = null;
//        Integer issueId = null;

        authentificate(token);

        checkIssue(issueId);

        saveImage(data);

    }

    private void authentificate(String token) {
        ticketControl.setHexTicket(token);
        userId = ticketControl.getIntValue();
    }

    private void checkIssue(Integer issueId) {

        rootComment = em.createQuery(
            "SELECT e FROM IssueComment e " +
                "WHERE e.issue.id=:issueId AND e.replyTo IS NULL", IssueCommentModel.class
        )
            .setParameter("issueId", issueId)
            .getSingleResult();

        if (!userId.equals(rootComment.getUser().getId())) {
            throw new RuntimeException("Issue mismatch");
        }
    }

    private void saveImage(byte[] data) {

        byte[] imageData = resize(data, 800, 600);

        em.getTransaction().begin();

        IssueCommentImageModel image = new IssueCommentImageModel();

//      @SequenceGenerator(name = "IssueCommentImageNumberGen", sequenceName = "issueimage_number_gen")
//      @GeneratedValue(generator = "IssueCommentImageNumberGen")

        Number number = (Number) em.createNativeQuery(
            "select gen_id( issueimage_number_gen, 1 ) from RDB$DATABASE"
        ).getSingleResult();


        image.setNumber(number.intValue());
        image.setComment(rootComment);
        image.setData(imageData);

        em.persist(image);

        em.getTransaction().commit();
    }

    private byte[] resize(byte data[], int maxWidth, int maxHeight) {

        BufferedImage image;

        try {
            image = ImageIO.read(new ByteArrayInputStream(data));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        double scale = Math.max(((double) image.getWidth()) / maxWidth, ((double) image.getHeight()) / maxHeight);

        if (scale > 1) {
            int targetWidth = (int) Math.round(((double) image.getWidth()) / scale);
            int targetHeight = (int) Math.round(((double) image.getHeight()) / scale);

            int type = image.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : image.getType();

            Image scaledImage = image.getScaledInstance(targetWidth, targetHeight, Image.SCALE_FAST);

            image = new BufferedImage(targetWidth, targetHeight, type);

            image.getGraphics().drawImage(scaledImage, 0, 0, null);
        }

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, "jpeg", output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return output.toByteArray();

    }


}

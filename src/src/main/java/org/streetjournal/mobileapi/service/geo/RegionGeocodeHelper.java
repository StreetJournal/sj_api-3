package org.streetjournal.mobileapi.service.geo;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: belan_000
 * Date: 09.10.13
 * Time: 20:32
 * To change this template use File | Settings | File Templates.
 */
public class RegionGeocodeHelper {

    private final static Logger logger = LoggerFactory.getLogger(RegionGeocodeHelper.class);


    public String getRegion(int lat, int lng) {

        try {

            logger.info("Geocode lat={} lng={}", lat, lng);

            Client c = Client.create();
            WebResource r = c.resource("http://opencity.in.ua/geo/regions/search");

            GeoRegions bean = r
                .queryParam("lat", String.valueOf(lat))
                .queryParam("lng", String.valueOf(lng))
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(GeoRegions.class);

            if (bean != null) {
                List<GeoRegion> regions = bean.getRegions();
                if (regions != null && regions.size() > 0) {
                    String result = regions.get(0).getCode();
                    logger.info("Geocode to [{}]", result);
                    return result;
                }
            }

        } catch (Exception e) {
            logger.error("Geocode error", e);
        }

        return "UA";

    }

}

package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Коды стран
 */
@Entity(name = "Country")
@Table(name = "country")
@SequenceGenerator(name="CountryIdGen", sequenceName = "country_id_gen")
public class CountryModel {

  private Integer id;

  private String code;

  @Id
  @GeneratedValue(generator="CountryIdGen")
  @Column(name = "country_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "country_code", nullable = false, columnDefinition="CHAR(2)")
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

}

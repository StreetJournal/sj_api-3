package org.streetjournal.mobileapi.service;

import org.junit.Before;
import org.junit.Test;
import org.streetjournal.mobileapi.config.Configuration;

import static org.junit.Assert.assertEquals;

public class UrlHelperTest {

    private UrlHelper urlHelper;

    @Before
    public void setUp() {
        urlHelper = new UrlHelper();
        Configuration configuration = new Configuration();
        configuration.host = "test.ru";
        urlHelper.setConfiguration(configuration);
    }

    @Test
    public void testGetImageUrlReturnsCorrectPathWithoutPreviewParameter() {
        String url = urlHelper.getImageUrl(10);

        assertEquals("http://test.ru/get_issue_image/10", url);
    }

    @Test
    public void testGetImageUrlReturnsCorrectPathWithoutPreviewParameterIfPreviewIsFalse() {
        String url = urlHelper.getImageUrl(10,false);

        assertEquals("http://test.ru/get_issue_image/10", url);
    }

    @Test
    public void testGetImageUrlReturnsCorrectPathWithPreviewParameterIfPreviewIsTrue() {
        String url = urlHelper.getImageUrl(10,true);

        assertEquals("http://test.ru/get_issue_image/10?preview=true", url);
    }
}

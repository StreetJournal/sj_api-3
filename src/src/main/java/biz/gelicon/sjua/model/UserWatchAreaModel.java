package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 */
@Entity(name = "UserWatchArea")
@Table(name = "watchareaproguser")
@SequenceGenerator(name = "UserWatchAreaIdGen", sequenceName = "watchareaproguser_id_gen")
public class UserWatchAreaModel {

  private Integer id;

  private UserModel user;

  private WatchAreaModel watchArea;

  private Boolean active;

  private String position;

  @Id
  @GeneratedValue(generator = "UserWatchAreaIdGen")
  @Column(name = "watchareaproguser_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @ManyToOne
  @JoinColumn(name = "proguser_id", nullable = false)
  public UserModel getUser() {
    return user;
  }

  public void setUser(UserModel user) {
    this.user = user;
  }

  @ManyToOne
  @JoinColumn(name = "watcharea_id", nullable = false)
  public WatchAreaModel getWatchArea() {
    return watchArea;
  }

  public void setWatchArea(WatchAreaModel watchArea) {
    this.watchArea = watchArea;
  }

  @Column(name = "watchareaproguser_active", nullable = false, columnDefinition = "integer")
  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  @Column(name = "watchareaproguser_position", nullable = true, length = 255)
  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

}

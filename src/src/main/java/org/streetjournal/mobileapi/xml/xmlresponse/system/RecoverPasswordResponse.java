package org.streetjournal.mobileapi.xml.xmlresponse.system;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 */
@XmlRootElement(name = "Response")
public class RecoverPasswordResponse extends AbstractResponse {

    public enum Status {

        OK("ok", "Recover link was sent"),

        NOT_FOUND("not_found", "Bad login");

        public final String code;
        public final String note;

        Status(String code, String note) {
            this.code = code;
            this.note = note;
        }

    }

    public RecoverPasswordResponse() {
    }

    public RecoverPasswordResponse(@NotNull Status status) {
        super(status.code, status.note);
    }

}

package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Ответ, содержащий идентификатор созданного комментария
 */
@XmlRootElement(name = "Response")
public class CreateIssueCommentResponse extends AbstractResponse {

    private Integer issueCommentId;

    public CreateIssueCommentResponse() {
        this(null);
    }

    public CreateIssueCommentResponse(Integer issueCommentId) {
        super(0, "Issue created");
        this.issueCommentId = issueCommentId;
    }

    @XmlElement(name = "IssueCommentId", required = true, nillable = false)
    public Integer getIssueCommentId() {
        return issueCommentId;
    }

    public void setIssueCommentId(Integer issueId) {
        this.issueCommentId = issueCommentId;
    }

}

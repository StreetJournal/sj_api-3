package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * Список идентификаторов проблем
 */
@XmlRootElement(name = "Response")
@XmlType(name = "IssueCreateResponseType")
public class IssueCreateResponse extends AbstractResponse {

  private Integer issueId;

  public IssueCreateResponse() {
    this(null);
  }

  public IssueCreateResponse(Integer issueId) {
    super(0, "Issue created");
    this.issueId = issueId;
  }

  @XmlElement(name = "IssueId", required = true, nillable = false)
  public Integer getIssueId() {
    return issueId;
  }

  public void setIssueId(Integer issueId) {
    this.issueId = issueId;
  }

}

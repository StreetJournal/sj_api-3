package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.IssueStatus;
import org.streetjournal.mobileapi.xml.IssueStatusAdapter;
import org.streetjournal.mobileapi.xml.TimestampAdapter;
import org.streetjournal.mobileapi.xml.xmlresponse.organization.OrganizationData;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Comment")
public class IssueCommentData {

    private Integer id;

    private Integer replyToId;

    private Date created;

    private AuthorData author;

    private IssueStatus status;

    private Boolean vote;

    private String text;

    private List<ImageData> images;

    private OrganizationData organization;

    @XmlAttribute(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlElement(name = "ReplyTo")
    public Integer getReplyToId() {
        return replyToId;
    }

    public void setReplyToId(Integer replyToId) {
        this.replyToId = replyToId;
    }

    @XmlJavaTypeAdapter(TimestampAdapter.class)
    @XmlElement(name = "Created")
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @XmlElementRef
    public AuthorData getAuthor() {
        return author;
    }

    public void setAuthor(AuthorData author) {
        this.author = author;
    }

    @XmlElement(name = "Text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @XmlElement(name = "Status")
    @XmlJavaTypeAdapter(IssueStatusAdapter.class)
    public IssueStatus getStatus() {
        return status;
    }

    public void setStatus(IssueStatus status) {
        this.status = status;
    }

    @XmlElement(name = "Vote")
    public Boolean getVote() {
        return vote;
    }

    public void setVote(Boolean vote) {
        this.vote = vote;
    }

    @XmlElementRef
    public List<ImageData> getImages() {
        return images;
    }

    public void setImages(List<ImageData> images) {
        this.images = images;
    }

    @XmlElementRef
    public OrganizationData getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationData organization) {
        this.organization = organization;
    }

}

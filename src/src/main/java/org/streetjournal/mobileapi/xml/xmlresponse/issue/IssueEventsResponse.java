package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Response")
public class IssueEventsResponse extends AbstractResponse {

    private List<IssueCommentData> issues;

    private List<IssueDispatchData> dispatches;

    public IssueEventsResponse() {
    }

    public IssueEventsResponse(List<IssueCommentData> issues, List<IssueDispatchData> dispatches) {
        super(0, "Events found");
        this.issues = issues;
        this.dispatches = dispatches;
    }

    @XmlElementRef
    public List<IssueCommentData> getIssues() {
        return issues;
    }

    @XmlElementRef
    public List<IssueDispatchData> getDispatches() {
        return dispatches;
    }

}

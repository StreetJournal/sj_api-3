package org.streetjournal.mobileapi.api.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 */

@Path("test")
public class TestHandler {

  private static final Logger logger = LoggerFactory.getLogger(TestHandler.class);

  @GET
  public String execute() {
    logger.info("Execute test handler");
    return "Hello, all!";
  }

}

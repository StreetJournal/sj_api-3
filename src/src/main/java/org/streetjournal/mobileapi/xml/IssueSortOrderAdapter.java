package org.streetjournal.mobileapi.xml;

/**
 *
 */
public class IssueSortOrderAdapter extends AbstractEnumAdapter<SortOrder> {

  public IssueSortOrderAdapter() {
    super(SortOrder.class);
  }

}

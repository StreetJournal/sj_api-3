package org.streetjournal.mobileapi.config;

import biz.gelicon.server.service.PasswordHash;
import com.google.inject.AbstractModule;

/**
 *
 */
public class ServiceModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(PasswordHash.class).to(SitePasswordHash.class);
  }

}

package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Модель связи проблемы со слоем (категорией)
 */
@Entity(name = "IssueLayerBind")
@Table(name = "issuelayer")
@SequenceGenerator(name = "IssueLayerBindIdGen", sequenceName = "issuelayer_id_gen")
public class IssueLayerBindModel {

  private Integer id;

  private IssueModel issue;

  private LayerModel layer;

  private Boolean main;

  @Id
  @GeneratedValue(generator = "IssueLayerBindIdGen")
  @Column(name = "issuelayer_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Проблема для связи
   *
   * @return
   */
  @ManyToOne
  @JoinColumn(name = "issue_id", nullable = false)
  public IssueModel getIssue() {
    return issue;
  }

  public void setIssue(IssueModel issue) {
    this.issue = issue;
  }

  /**
   * Слой связи
   *
   * @return
   */
  @ManyToOne
  @JoinColumn(name = "layer_id", nullable = false)
  public LayerModel getLayer() {
    return layer;
  }

  public void setLayer(LayerModel layer) {
    this.layer = layer;
  }

  /**
   * Признак основной связи (true) или дополнительной (false)
   *
   * @return
   */
  @Column(name = "issuelayer_mainflag", columnDefinition = "integer", nullable = false)
  public Boolean isMain() {
    return main;
  }

  public void setMain(Boolean main) {
    this.main = main;
  }

}

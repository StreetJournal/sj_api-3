package org.streetjournal.mobileapi.xml;

/**
 *
 */
public enum SortOrder {

  DATE,

  VOTES,

  DISTANCE

}

package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Текст проблемы и Комментарий к проблеме
 *
 * Текст проблемы находится в корне дерева комментариев и имеет ownerissuecomment_id (replyTo),
 * установленное в NULL
 */
@Entity(name = "IssueComment")
@Table(name = "issuecomment")
@SequenceGenerator(name = "IssueCommentIdGen", sequenceName = "issuecomment_id_gen")
public class IssueCommentModel {

    public static final int STATUS_NOSPAM = -1;

    public static final int STATUS_NORMAL = 0;

    public static final int STATUS_DELETED = 1;

    private Integer id;

    private IssueModel issue;

    private IssueCommentModel replyTo;

    private IssueCommentTypeModel type;

    private IssueStatusModel issueStatus;

    private UserModel user;

    private Date creationDate;

    private Date modificationDate;

    private String text;

    private Integer deleteStatus;

    private Boolean anonymous;

    private WatchAreaModel organization;

    @Id
    @GeneratedValue(generator = "IssueCommentIdGen")
    @Column(name = "issuecomment_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "issue_id")
    public IssueModel getIssue() {
        return issue;
    }

    public void setIssue(IssueModel issue) {
        this.issue = issue;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ownerissuecomment_id", nullable = true)
    public IssueCommentModel getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(IssueCommentModel replyTo) {
        this.replyTo = replyTo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "proguser_id", nullable = false)
    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "issuecomment_datecreate", nullable = false)
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "issuecomment_dateupdate", nullable = false)
    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Lob
    @Column(name = "issuecomment_text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Column(name = "issuecomment_deleteflag", nullable = false)
    public Integer getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Integer deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "issuecommenttype_id", nullable = false)
    public IssueCommentTypeModel getType() {
        return type;
    }

    public void setType(IssueCommentTypeModel type) {
        this.type = type;
    }

    @Column(name = "issuecomment_anonymflag", columnDefinition = "integer", nullable = false)
    public Boolean getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Boolean anonymous) {
        this.anonymous = anonymous;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "issuestatus_id", nullable = true)
    public IssueStatusModel getIssueStatus() {
        return issueStatus;
    }

    public void setIssueStatus(IssueStatusModel issueStatus) {
        this.issueStatus = issueStatus;
    }

    /**
     * Если сотрудник перед написанием комментария выбрал отганизацию, то эта организация
     * запоминается здесь.
     *
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "watcharea_id", nullable = true)
    public WatchAreaModel getOrganization() {
        return organization;
    }

    public void setOrganization(WatchAreaModel organization) {
        this.organization = organization;
    }

}

package org.streetjournal.mobileapi.api.layers;

import biz.gelicon.sjua.model.LayerModel;
import biz.gelicon.sjua.model.LayerNameModel;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.config.Configuration;
import org.streetjournal.mobileapi.service.LocaleHelper;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.Localized;
import org.streetjournal.mobileapi.xml.xmlrequest.LayerListRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.CategoryData;
import org.streetjournal.mobileapi.xml.xmlresponse.CategoryGroupData;
import org.streetjournal.mobileapi.xml.xmlresponse.LayerData;
import org.streetjournal.mobileapi.xml.xmlresponse.LayerListResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@javax.ws.rs.Path("get_layer_list")
public class LayerListHandler {

    private static final Logger logger = LoggerFactory.getLogger(LayerListHandler.class);

    @Inject
    private EntityManager em;
    @Inject
    private LocaleHelper localeHelper;
    @Inject
    private Configuration configuration;

    private final List<LayerData> result = new ArrayList<>();
    private final Map<LayerModel, List<LayerNameModel>> map = new HashMap<>();

    private LayerListRequest request;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public AbstractResponse execute(LayerListRequest request) {

        this.request = request.setLocale(localeHelper.normalize(request.getLocale()));

        cacheNames();

        prepareResult();

        return new LayerListResponse(result);

    }

    private void cacheNames() {

        for (LayerNameModel item : em.createQuery("SELECT e FROM LayerName e",
                LayerNameModel.class).getResultList()) {

            List<LayerNameModel> list = map.get(item.getLayer());

            if (list == null) {
                map.put(item.getLayer(), list = new ArrayList<>());
            }

            list.add(item);

        }
    }

    //TODO: LayerData, CategoryGroupData и CategoryData используются только здесь, для исключение копипаста прошлого программиста необходимо сделать общую абстракцию
    private void prepareResult() {
        List<LayerModel> items;

        items = em.createQuery("SELECT e FROM Layer e WHERE e.status=1 ORDER BY e.order", LayerModel.class).getResultList();


        Map<Integer, LayerData> layerMap = new HashMap<>();

        for (LayerModel item : items) {

            if (item.getParent() != null && item.getParent() != item) {
                continue;
            }
            if (!isCategoryGroupShouldBeIncluded(item)) {
                continue;
            }

            LayerData data = new LayerData();

            data.setCode(item.getCode());
            data.setType(item.getType().getId());

            data.setName(item.getName());

            data.setNote(item.getNote());

            data.setIcon(item.getIconData());

            List<LayerNameModel> names = map.get(item);

            if (names != null) {
                if (null != request.getLocale()) {
                    data.setNames(null);
                    LayerNameModel nameModel = Iterables.find(names, new Predicate<LayerNameModel>() {
                        @Override
                        public boolean apply(LayerNameModel nameModel) {
                            return request.getLocale().getLanguage().equals(nameModel.getLocale().getCode());
                        }
                    }, null);
                    if (null != nameModel) {
                        data.setName(nameModel.getName());
                    }
                } else {
                    for (LayerNameModel name : names) {
                        data.getNames().add(new Localized(name.getLocale().getCode(), name.getName()));
                    }
                }
            }

            layerMap.put(item.getId(), data);

            result.add(data);

        }

        Map<Integer, CategoryGroupData> groupMap = new HashMap<>();

        for (LayerModel item : items) {

            if (item.getParent() == null || item.getParent() == item) {
                continue;
            }

            LayerData layer = layerMap.get(item.getParent().getId());
            //TODO: этот код примерно тот же код, что используется на permroad. Необходимо делать ограничение в SQL, а не так, как сделано тут
            if (layer == null || (
                    null != configuration.categoryGroupCodes &&
                            !configuration.categoryGroupCodes.isEmpty() &&
                            !isCategoryGroupShouldBeIncluded(item))
                    ) {
                continue;
            }

            CategoryGroupData data = new CategoryGroupData();

            data.setCode(item.getCode());

            data.setName(item.getName());

            data.setNote(item.getNote());

            data.setIcon(item.getIconData());

            layer.getGroups().add(data);

            List<LayerNameModel> names = map.get(item);

            if (names != null) {
                if (null != request.getLocale()) {
                    data.setNames(null);
                    LayerNameModel nameModel = Iterables.find(names, new Predicate<LayerNameModel>() {
                        @Override
                        public boolean apply(LayerNameModel nameModel) {
                            return request.getLocale().getLanguage().equals(nameModel.getLocale().getCode());
                        }
                    }, null);
                    if (null != nameModel) {
                        data.setName(nameModel.getName());
                    }
                } else {
                    for (LayerNameModel name : names) {
                        data.getNames().add(new Localized(name.getLocale().getCode(), name.getName()));
                    }
                }
            }

            groupMap.put(item.getId(), data);

        }

        for (LayerModel item : items) {

            CategoryGroupData group = groupMap.get(item.getParent().getId());

            if (group == null) {
                continue;
            }

            CategoryData data = new CategoryData();

            data.setCode(item.getCode());

            data.setName(item.getName());

            data.setNote(item.getNote());

            data.setIcon(item.getIconData());

            List<LayerNameModel> names = map.get(item);

            if (names != null) {
                if (null != request.getLocale()) {
                    data.setNames(null);
                    LayerNameModel nameModel = Iterables.find(names, new Predicate<LayerNameModel>() {
                        @Override
                        public boolean apply(LayerNameModel nameModel) {
                            return request.getLocale().getLanguage().equals(nameModel.getLocale().getCode());
                        }
                    }, null);
                    if (null != nameModel) {
                        data.setName(nameModel.getName());
                    }
                } else {
                    for (LayerNameModel name : names) {
                        data.getNames().add(new Localized(name.getLocale().getCode(), name.getName()));
                    }
                }
            }

            group.getCategories().add(data);

        }

    }

    //быстрый хак, нормальный код будет в новой версии апи.
    private boolean isCategoryGroupShouldBeIncluded(LayerModel layer) {
        if (configuration.categoryGroupCodes == null) {
            return true;
        }
        for (String code : configuration.categoryGroupCodes) {
            Integer c = Integer.valueOf(code);
            if (layer.getId().equals(c) ||
                    (layer.getParent() != null && layer.getParent().getId().equals(c)) ||
                    (layer.getParent() != null && layer.getParent().getParent() != null && layer.getParent().getParent().getId().equals(c)) ||
                    (layer.getParent() != null && layer.getParent().getParent() != null && layer.getParent().getParent().getParent() != null && layer.getParent().getParent().getParent().getId().equals(c)) ||
                    (layer.getParent() != null && layer.getParent().getParent() != null && layer.getParent().getParent().getParent() != null && layer.getParent().getParent().getParent().getParent() != null && layer.getParent().getParent().getParent().getParent().getId().equals(c)))
            {
                return true;
            }
        }
        return false;
    }

}

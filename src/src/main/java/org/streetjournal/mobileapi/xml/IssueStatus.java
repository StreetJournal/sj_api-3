package org.streetjournal.mobileapi.xml;

/**
 *
 */
public enum IssueStatus {

    OPEN,

    ACCEPTED,

    CLOSED,

    ARCHIVED,

    PREVIEW

}

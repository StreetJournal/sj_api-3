package org.streetjournal.mobileapi.config;

import biz.gelicon.server.config.DatabaseConfig;
import biz.gelicon.server.config.TicketConfig;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 *
 */
public class ConfigurationModule extends AbstractModule {

  private static final Logger logger = LoggerFactory.getLogger(ConfigurationModule.class);

  private final File configFile;

  public ConfigurationModule(File configFile) {
    this.configFile = configFile;
  }

  @Override
  protected void configure() {
  }

  /**
   * @return
   */
  @Provides
  @Singleton
  public Configuration getConfiguration() {

    if (!configFile.exists()) {
      logger.warn("Configutation file [{}] not exists", configFile.getAbsoluteFile());
      return null;
    }

    logger.info("Reading configuration from [{}]", configFile.getPath());

    try {

      JAXBContext jc = JAXBContext.newInstance(Configuration.class);

      Unmarshaller marshaller = jc.createUnmarshaller();

      Configuration config = (Configuration) marshaller.unmarshal(configFile);

      return config;

    } catch (JAXBException e) {
      throw new RuntimeException(e);
    }

  }

  @Provides
  @Singleton
  public TicketConfig getTicketConfig(Configuration configuration) {
    return configuration.security;
  }

  @Provides
  @Singleton
  public DatabaseConfig getDatabaseConfig(Configuration configuration) {
    return configuration.database;
  }

}

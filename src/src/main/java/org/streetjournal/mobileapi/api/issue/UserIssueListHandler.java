package org.streetjournal.mobileapi.api.issue;

import biz.gelicon.server.service.TicketControl;
import biz.gelicon.sjua.model.IssueCommentModel;
import biz.gelicon.sjua.model.IssueModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.issue.UserIssueListRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.IssueData;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.IssueDataResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@javax.ws.rs.Path("get_user_issue_list")
public class UserIssueListHandler {

    private static final Logger logger = LoggerFactory.getLogger(UserIssueListHandler.class);

    @Inject
    EntityManager em;

    @Inject
    CriteriaBuilder cb;

    @Inject
    TicketControl ticketControl;

    private List<IssueData> result;

    private UserIssueListRequest request;

    private Integer userId;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces({MediaType.APPLICATION_XML})
    public AbstractResponse execute(UserIssueListRequest request) {

        this.request = request;

        authentificate();

        prepareResult();

        return new IssueDataResponse(result);
    }

    private void prepareResult() {

        boolean useSerial = request.getUseSerial() != null && request.getUseSerial();

        CriteriaQuery<Tuple> q = cb.createTupleQuery();

        Root<IssueCommentModel> root = q.from(IssueCommentModel.class);

        if (useSerial) {
            q.select(cb.tuple(root.get("issue").get("id"), root.get("modificationDate"), root.get("issue").get("moderationDate")));
        } else {
            q.select(cb.tuple(root.get("issue").get("id")));
        }

        Path<IssueModel> issue = root.get("issue");

        applyFilter(q, cb.isNull(root.get("replyTo")), root, issue);
        applySort(q, root);

        TypedQuery<Tuple> query = em.createQuery(q);

        query.setFirstResult(0);

        int limit = request.getLimit() != null ? Math.min(request.getLimit(), 1000) : 1000;

        query.setMaxResults(limit);

        result = new ArrayList<>();

        for (Tuple item : query.getResultList()) {

            IssueData data = new IssueData();

            data.setId(item.get(0, Integer.class));

            if (useSerial) {

                Date date1 = item.get(1, Date.class);
                Date date2 = item.get(2, Date.class);

                long serial;

                if (date2 != null && date1.after(date2)) {
                    serial = date1.getTime();
                } else {
                    serial = date2.getTime();
                }

                data.setSerial(serial);

            }

            result.add(data);

        }

    }

    private void applyFilter(CriteriaQuery<?> query, Predicate filter, Root<IssueCommentModel> rootComment, Path<IssueModel> root) {

        List<Expression<Boolean>> result = new ArrayList<>();

        if (filter != null) {
            result.add(filter);
        }

        // Проверка на модерацию
//        result.add(cb.equal(root.get("mailMode"), IssueModel.CANEMAIL_PUBLISHED));

        result.add(cb.equal(rootComment.get("user").get("id"), userId));

        if (result.size() > 0) {
            Predicate ands = cb.conjunction();
            ands.getExpressions().addAll(result);
            query.where(ands);
        }

    }

    private void applySort(CriteriaQuery<?> query, Path<IssueCommentModel> root) {
        Map<String, Path<?>> result = new HashMap<>();
        applySort(query, result, cb.desc(root.get("creationDate")));
    }

    private void applySort(CriteriaQuery<?> query, Map<String, Path<?>> map, Order defaultOrder) {
        List<Order> orders = new ArrayList<>();

        if (orders.size() > 0) {
            query.orderBy(orders);
        } else {
            query.orderBy(defaultOrder);
        }
    }

    private void authentificate() {
        ticketControl.setHexTicket(request.getToken());
        userId = ticketControl.getIntValue();
    }

}

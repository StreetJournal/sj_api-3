package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

// @TODO Нужно ли для типа карты иметь отдельный справочник?
/**
 * Тип карты
 */
@Entity(name = "MapType")
@Table(name = "maptype")
@SequenceGenerator(name="MapTypeIdGen", sequenceName = "maptype_id_gen")
public class MapTypeModel {

  private Integer id;

  private String name;

  @Id
  @GeneratedValue(generator="MapTypeIdGen")
  @Column(name = "maptype_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "maptype_name", nullable = false, length = 30)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}

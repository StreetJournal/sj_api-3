package org.streetjournal.mobileapi.api;

/**
 *
 */

import org.streetjournal.mobileapi.xml.AbstractRequest;
import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.io.OutputStream;

public abstract class BasicJaxRsHandler<Request extends AbstractRequest, Response extends AbstractResponse> {

  protected Request request;

  protected Response response;

  protected BasicJaxRsHandler(Class<Request> requestClass, Class<Response> responseClass) {

  }

  @POST
  public final AbstractResponse execute(Request request) {

    this.request = request;

    execute();

    return response;

  }

  protected abstract void execute();

  @GET
  @Path("request")
  public OutputStream getRequestDescription() {
    return null;
  }

  @GET
  @Path("response")
  public OutputStream getResponseDescription() {
    return null;
  }

}

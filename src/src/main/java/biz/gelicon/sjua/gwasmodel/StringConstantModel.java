package biz.gelicon.sjua.gwasmodel;

import javax.persistence.*;

/**
 *
 */
@Entity(name = "StringConstant")
@Table(name = "cv_string")
public class StringConstantModel {

  private Integer id;

  private ConstantValueModel owner;

  private String value;

  @Id
  @Column(name = "constantvalue_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @OneToOne
  @PrimaryKeyJoinColumn
  public ConstantValueModel getOwner() {
    return owner;
  }

  public void setOwner(ConstantValueModel owner) {
    this.owner = owner;
  }

  @Column(name="cv_value", nullable = false, columnDefinition = "CHAR(255)", length = 255)
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

}

package org.streetjournal.mobileapi.config;

import com.google.inject.AbstractModule;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Path;

/**
 *
 */
public class JaxRsHandlersModule extends AbstractModule {

  private static final Logger logger = LoggerFactory.getLogger(JaxRsHandlersModule.class);

  private static final String HANDLER_PREFIX = "org.streetjournal.mobileapi.api";

  @Override
  protected void configure() {

    logger.info("Setup JAX-RS handlers");

    Reflections reflections = new Reflections(HANDLER_PREFIX);

    int counter = 0;

    for (Class<?> item : reflections.getTypesAnnotatedWith(Path.class)) {

      logger.info("Bind JAXRS handler=[{}]", item.getName());

      bind(item);

      counter++;

    }

    logger.info("Found {} JAXRS handler(s)", counter);

    bind(HandlerExceptionMapper.class);

  }

}

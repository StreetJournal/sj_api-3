package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 */
@XmlRootElement(name = "Image")
public class ImageData {

  private Integer id;

  private Integer order;

  private String url;

  private String previewUrl;

  public ImageData() {
  }

  @XmlAttribute(name = "id")
  public Integer getId() {
    return id;
  }

  public ImageData setId(Integer id) {
    this.id = id;
    return this;
  }

  @XmlAttribute(name = "order")
  public Integer getOrder() {
    return order;
  }

  public ImageData setOrder(Integer order) {
    this.order = order;
    return this;
  }

  @XmlElement(name = "Url")
  public String getUrl() {
    return url;
  }

  public ImageData setUrl(String url) {
    this.url = url;
    return this;
  }

  @XmlElement(name = "PreviewUrl")
  public String getPreviewUrl() {
    return previewUrl;
  }

  public ImageData setPreviewUrl(String previewUrl) {
    this.previewUrl = previewUrl;
    return this;
  }
}

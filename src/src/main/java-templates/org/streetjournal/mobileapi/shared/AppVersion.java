package org.streetjournal.mobileapi.shared;

public class AppVersion {

  public static final String VERSION = "${project.version}";
		  
  public static final String TIMESTAMP = "${build.timestamp}";

}
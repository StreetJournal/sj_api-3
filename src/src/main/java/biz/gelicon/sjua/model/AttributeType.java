package biz.gelicon.sjua.model;

public enum AttributeType {
    EMPTY, STRING, INTEGER, REAL, DATE, BOOLEAN,  DICTIONARY, MULTIPLE_CHOISE_DICTIONARY
}

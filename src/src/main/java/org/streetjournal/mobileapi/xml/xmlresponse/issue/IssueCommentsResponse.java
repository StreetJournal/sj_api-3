package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Response")
public class IssueCommentsResponse extends AbstractResponse {

    private List<IssueCommentData> issues;

    public IssueCommentsResponse() {
    }

    public IssueCommentsResponse(List<IssueCommentData> issues) {
        super(0, "Comments found");
        this.issues = issues;
    }

    @XmlElementRef
    public List<IssueCommentData> getIssues() {
        return issues;
    }

}

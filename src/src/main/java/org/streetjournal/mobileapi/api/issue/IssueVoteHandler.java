package org.streetjournal.mobileapi.api.issue;

import biz.gelicon.server.service.TicketControl;
import biz.gelicon.sjua.model.IssueCommentModel;
import biz.gelicon.sjua.model.IssueCommentTypeModel;
import biz.gelicon.sjua.model.IssueModel;
import biz.gelicon.sjua.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.service.IssueHelper;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.issue.IssueVoteRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.IssueVoteResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Date;

/**
 *
 */
@Path("vote_issue")
public class IssueVoteHandler {

    private static final Logger logger = LoggerFactory.getLogger(IssueVoteHandler.class);

    @Inject
    EntityManager em;

    @Inject
    TicketControl ticketControl;

    @Inject
    IssueHelper issueHelper;

    private IssueVoteRequest request;

    private Integer userId;

    private IssueModel issue;

    private UserModel user;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public AbstractResponse execute(IssueVoteRequest request) {

        this.request = request;

        authentificate();

        issue = em.getReference(IssueModel.class, request.getIssueId());
        user = em.getReference(UserModel.class, userId);

        em.getTransaction().begin();

        if (vote()) {
            em.getTransaction().commit();
            return new IssueVoteResponse(0, "Vote success");
        } else {
            em.getTransaction().rollback();
            return new IssueVoteResponse(0, "Vote not consider");
        }
    }

    private void authentificate() {
        ticketControl.setHexTicket(request.getToken());
        userId = ticketControl.getIntValue();
    }

    private boolean vote() {

        Date now = new Date();

        IssueCommentModel rootComment = issueHelper.startUpdate(issue);

        if (rootComment.getUser() == user) {
            return false;
        }

        if (getVotesCount() > 0) {
            return false;
        }

        IssueCommentModel issueComment = new IssueCommentModel();

        issueComment.setIssue(issue);
        issueComment.setReplyTo(rootComment);
        issueComment.setUser(user);
        issueComment.setType(em.getReference(IssueCommentTypeModel.class, IssueCommentTypeModel.TYPE_VOTE));
        issueComment.setAnonymous(false);
        issueComment.setDeleteStatus(IssueCommentModel.STATUS_NORMAL);

        issueComment.setCreationDate(now);
        issueComment.setModificationDate(now);

        em.persist(issueComment);

        em.createNativeQuery("EXECUTE PROCEDURE ISSUERATINGUPD ?").setParameter(1, issue.getId()).executeUpdate();

        return true;
    }

    private int getVotesCount() {

        Long result = em.createQuery(
            "SELECT count(*) "
                + "FROM IssueComment e "
                + "WHERE e.type=:type AND e.issue=:issue AND e.user=:user",
            Long.class
        )
            .setParameter("type", em.getReference(IssueCommentTypeModel.class, IssueCommentTypeModel.TYPE_VOTE))
            .setParameter("issue", issue)
            .setParameter("user", user)
            .getSingleResult();

        return result.intValue();
    }

}

package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Канал взаимодействия
 */
@Entity(name = "CommunicationTypeType")
@Table(name = "communicationtype")
@SequenceGenerator(name = "CommunicationTypeIdGen", sequenceName = "communicationtype_id_gen")
public class CommunicationTypeModel {

  private Integer id;

  private String name;

  @Id
  @GeneratedValue(generator = "CommunicationTypeIdGen")
  @Column(name = "communicationtype_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Название типа организации (нелокализованное)
   *
   * @return
   */
  @Column(name = "communicationtype_name", length = 50, nullable = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}

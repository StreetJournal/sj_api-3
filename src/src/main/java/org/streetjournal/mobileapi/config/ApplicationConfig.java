package org.streetjournal.mobileapi.config;

import biz.gelicon.server.modules.DatabaseModule;
import biz.gelicon.server.modules.ManagedScopeModule;
import biz.gelicon.server.modules.SecurityModule;
import biz.gelicon.server.service.ManagedFilter;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.shared.AppVersion;

import javax.servlet.ServletContextEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class ApplicationConfig extends GuiceServletContextListener {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);

    private static final String SERVICE_CONFIG = "/WEB-INF/serviceconfig.xml";

    private Injector injector;

    private static final String PERSISTENCE_NAME = "site";

    @Override
    public void contextInitialized(ServletContextEvent event) {

        logger.info("Initialization " + AppVersion.VERSION + " " + AppVersion.TIMESTAMP);

        injector = Guice.createInjector(

                new ManagedScopeModule(),

                new DatabaseModule(PERSISTENCE_NAME),

                new JaxRsHandlersModule(),

                new SecurityModule(),

                new ServiceModule(),

                new ShapeBoundsModule(),

                new ConfigurationModule(
                        new File(event.getServletContext().getRealPath(SERVICE_CONFIG))
                ),

                new ServletModule() {

                    @Override
                    protected void configureServlets() {

                        Map<String, String> jerseyParams = new HashMap<>();

                        jerseyParams.put("com.sun.jersey.config.feature.Formatted", "true");

                        serve("/api/*").with(GuiceContainer.class, jerseyParams);

                        filter("/api/*").through(ManagedFilter.class);

                    }

                }
        );

        super.contextInitialized(event);

        logger.debug("Initialized");

    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {

        logger.debug("Destroing");

        super.contextDestroyed(event);

        logger.info("Destroyed");

    }

    /**
     * Требуется для GuiceServletContextListener
     */
    @Override
    protected Injector getInjector() {
        return injector;
    }

}

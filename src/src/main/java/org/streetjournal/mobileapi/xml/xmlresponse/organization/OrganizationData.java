package org.streetjournal.mobileapi.xml.xmlresponse.organization;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 */
@XmlRootElement(name = "Organization")
public class OrganizationData {

    private Integer id;

    private String name;

    public OrganizationData() {
    }

    public OrganizationData(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @XmlAttribute(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlValue
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

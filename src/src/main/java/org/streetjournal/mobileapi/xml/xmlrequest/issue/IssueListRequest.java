package org.streetjournal.mobileapi.xml.xmlrequest.issue;

import org.streetjournal.mobileapi.xml.*;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.AuthorData;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

/**
 * Зарос списка идентификаторов проблем, подпадающих под указанные критерии и отсортированных
 * в указанном порядке.
 */
@XmlRootElement(name = "Request")
public class IssueListRequest extends AbstractRequest {

    private String token;

    private Integer limit;

    private Boolean useSerial;

    private String region;

    private List<IssueStatus> status;

    private List<ModerationStatus> review;

    private List<String> categories;

    private List<AuthorData> authors;

    private SortOrder order;

    @XmlAttribute(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlElement(name = "Limit")
    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @XmlElement(name = "UseSerial")
    public Boolean getUseSerial() {
        return useSerial;
    }

    public void setUseSerial(Boolean useSerial) {
        this.useSerial = useSerial;
    }

    @XmlElement(name = "Region")
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @XmlElement(name = "Status")
    @XmlJavaTypeAdapter(IssueStatusAdapter.class)
    public List<IssueStatus> getStatus() {
        return status;
    }

    public void setStatus(List<IssueStatus> status) {
        this.status = status;
    }

    @XmlElement(name = "Author")
    public List<AuthorData> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorData> authors) {
        this.authors = authors;
    }

    @XmlElement(name = "Review")
    @XmlJavaTypeAdapter(ModerationStatusAdapter.class)
    public List<ModerationStatus> getReview() {
        return review;
    }

    public void setReview(List<ModerationStatus> review) {
        this.review = review;
    }

    @XmlElement(name = "Category")
    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @XmlElement(name = "Order")
    @XmlJavaTypeAdapter(IssueSortOrderAdapter.class)
    public SortOrder getOrder() {
        return order;
    }

    public void setOrder(SortOrder order) {
        this.order = order;
    }

}

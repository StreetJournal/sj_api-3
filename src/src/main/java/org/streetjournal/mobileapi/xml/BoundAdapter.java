package org.streetjournal.mobileapi.xml;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 */
public class BoundAdapter extends XmlAdapter<String, Bound> {

    @Override
    public Bound unmarshal(String value) throws Exception {
        String[] words = value.split(" ");
        int south = Integer.parseInt(words[0]);
        int west = Integer.parseInt(words[1]);
        int north = Integer.parseInt(words[2]);
        int east = Integer.parseInt(words[3]);
        return new Bound(south, west, north, east);
    }

    @Override
    public String marshal(Bound value) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb
            .append(value.south).append(' ')
            .append(value.west).append(' ')
            .append(value.north).append(' ')
            .append(value.east);
        return sb.toString();
    }

}

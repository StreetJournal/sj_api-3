package org.streetjournal.mobileapi.xml.xmlrequest.system;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Данные для запроса на вход
 */
@XmlRootElement(name = "Request")
public class RecoverPasswordRequest extends AbstractRequest {

    private String email;

    public RecoverPasswordRequest() {
    }

    public RecoverPasswordRequest(String email) {
        this.email = email;
    }

    @XmlElement(name = "Email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}

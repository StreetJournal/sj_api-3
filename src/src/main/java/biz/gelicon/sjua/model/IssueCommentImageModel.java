package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 */
@Entity(name = "IssueCommentImage")
@Table(name = "issueimage")
@SequenceGenerator(name = "IssueCommentImageIdGen", sequenceName = "issueimage_id_gen")
public class IssueCommentImageModel {

    private Integer id;

    private Integer number;

    private IssueCommentModel comment;

    private byte[] data;

    @Id
    @GeneratedValue(generator = "IssueCommentImageIdGen")
    @Column(name = "issueimage_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "issueimage_number", nullable = false)
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "issuecomment_id", nullable = false)
    public IssueCommentModel getComment() {
        return comment;
    }

    public void setComment(IssueCommentModel comment) {
        this.comment = comment;
    }

    // @TODO почему-то в базе nullable == true
    @Lob
    @Column(name = "issueimage_blob", nullable = true)
    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

}

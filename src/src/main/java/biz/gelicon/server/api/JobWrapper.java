package biz.gelicon.server.api;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Injector;

public class JobWrapper implements Runnable {

  private static final Logger logger = LoggerFactory.getLogger(JobWrapper.class);

  private Class<? extends Runnable> jobClass;

  @Inject
  Injector injector;

//  @Inject
//  EntityManagerHelper helper;

  public Class<? extends Runnable> getJobClass() {
    return jobClass;
  }

  public void setJobClass(Class<? extends Runnable> jobClass) {
    this.jobClass = jobClass;
  }

  @Override
  public void run() {

    logger.info("Execute job class={}", jobClass.getName());

    try {

      try {
        injector.getInstance(jobClass).run();
      } finally {
//        helper.closeEntityManager();
      }

      logger.info("Job class={} completed", jobClass.getName());

    } catch (Exception e) {
      logger.error("Job execution error class={}", jobClass.getName(), e);
    }

  }

}

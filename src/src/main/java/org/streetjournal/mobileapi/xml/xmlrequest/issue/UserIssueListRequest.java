package org.streetjournal.mobileapi.xml.xmlrequest.issue;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Зарос списка идентификаторов проблем, подпадающих под указанные критерии и отсортированных
 * в указанном порядке.
 */
@XmlRootElement(name = "Request")
public class UserIssueListRequest extends AbstractRequest {

    private String token;

    private Integer limit;

    private Boolean useSerial;

    @XmlAttribute(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlElement(name = "Limit")
    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @XmlElement(name = "UseSerial")
    public Boolean getUseSerial() {
        return useSerial;
    }

    public void setUseSerial(Boolean useSerial) {
        this.useSerial = useSerial;
    }

}

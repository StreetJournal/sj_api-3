package org.streetjournal.mobileapi.config;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.regrect.RegionBoundsHelper;
import org.streetjournal.mobileapi.regrect.ShapeRectList;
import org.streetjournal.mobileapi.xml.Bound;

import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class ShapeBoundsModule extends AbstractModule {

    private static final Logger logger = LoggerFactory.getLogger(ShapeBoundsModule.class);

    @Override
    protected void configure() {
    }

    @Singleton
    @Provides
    public RegionBoundsHelper getBoundsHelper(Configuration configuration) {

        if (configuration.shapeFileConfig == null) {
            logger.error("Empty shape bounds file configuration");
            return null;
        }

        try {

            JAXBContext jaxb = JAXBContext.newInstance(ShapeRectList.class);

            Unmarshaller unmarshaller = jaxb.createUnmarshaller();

            logger.info("Loading shape bounds from file [{}]", configuration.shapeFileConfig.regionBoundsFile);

            ShapeRectList list = (ShapeRectList) unmarshaller.unmarshal(
                new File(configuration.shapeFileConfig.regionBoundsFile)
            );

            logger.info("Loaded shape bounds count={}", list.bounds.size());

            Map<String, Bound> map = new HashMap<>();

            for (ShapeRectList.RegionBound item : list.bounds) {
                map.put(item.code, item.bound);
            }

            return new RegionBoundsHelper(map);

        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}

package org.streetjournal.mobileapi.xml;

/**
 *
 */
public class IssueStatusAdapter extends AbstractEnumAdapter<IssueStatus>  {

    public IssueStatusAdapter() {
        super(IssueStatus.class);
    }

}

package biz.gelicon.sjua.gwasmodel;

import javax.persistence.*;

/**
 *
 */
@Entity(name = "CapResource")
@Table(name = "capresource")
public class CapResourceModel {

  private Integer id;

  private String code;

  @Id
  @Column(name = "capresource_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "capresource_code", columnDefinition = "CHAR(10)", nullable = true, length = 10)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

}

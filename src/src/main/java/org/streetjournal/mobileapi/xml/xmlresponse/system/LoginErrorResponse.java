package org.streetjournal.mobileapi.xml.xmlresponse.system;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 */
@XmlRootElement(name = "Response")
public class LoginErrorResponse extends AbstractResponse {

  public LoginErrorResponse(String code, String note) {
    super(1, code, note);
  }

}

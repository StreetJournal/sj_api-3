package biz.gelicon.sjua.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Коды стран
 */
@Entity(name = "User")
@Table(name = "proguser")
@SequenceGenerator(name = "UserIdGen", sequenceName = "proguser_id_gen")
public class UserModel {

  private Integer id;

  private String login;

  private String email;

  private String password;

//  private String region;

  private String name;

  private String surname;

  private String patronymic;

  private String phone;

  private String note;

  private String remark;

  private Date registrationDate;

  private Date lastVisitDate;

  private Date birthday;

  private Integer status;

  private Boolean autoSubscribe;

  private ToponymModel toponym;

  @Id
  @GeneratedValue(generator = "UserIdGen")
  @Column(name = "proguser_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "proguser_login", nullable = false, length = 50)
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  @Column(name = "proguser_email", length = 100)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Column(name = "proguser_password", length = 50)
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Column(name = "proguser_name", length = 100)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Column(name = "proguser_lastname", length = 100)
  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Column(name = "proguser_surname", length = 100)
  public String getPatronymic() {
    return patronymic;
  }

  public void setPatronymic(String patronymic) {
    this.patronymic = patronymic;
  }

  @Column(name = "proguser_regdate", nullable = false)
  public Date getRegistrationDate() {
    return registrationDate;
  }

  public void setRegistrationDate(Date registrationDate) {
    this.registrationDate = registrationDate;
  }

  @Column(name = "proguser_lastdate", nullable = false)
  public Date getLastVisitDate() {
    return lastVisitDate;
  }

  public void setLastVisitDate(Date lastVisitDate) {
    this.lastVisitDate = lastVisitDate;
  }

  @Column(name = "proguser_status", nullable = false)
  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Column(name = "proguser_autosubscribe", nullable = false, columnDefinition = "INTEGER")
  public Boolean getAutoSubscribe() {
    return autoSubscribe;
  }

  public void setAutoSubscribe(Boolean autoSubscribe) {
    this.autoSubscribe = autoSubscribe;
  }

//  @Column(name = "proguser_region", length = 255)
//  public String getRegion() {
//    return region;
//  }
//
//  public void setRegion(String region) {
//    this.region = region;
//  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "toponym_id", nullable = true)
  public ToponymModel getToponym() {
    return toponym;
  }

  public void setToponym(ToponymModel toponym) {
    this.toponym = toponym;
  }

  @Temporal(TemporalType.TIMESTAMP)
//  @Temporal(TemporalType.DATE)
  @Column(name = "proguser_birthday")
  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  @Column(name = "proguser_phone", length = 50)
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Lob
  @Column(name="proguser_note", length = 255)
  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  @Column(name = "proguser_remark", length = 255)
  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

}

package biz.gelicon.sjua.gwasmodel;

import javax.persistence.*;
import java.util.Date;

/**
 *
 */
@Entity(name = "ConstantValue")
@Table(name = "constantvalue")
public class ConstantValueModel {

  private Integer id;

  private ConstantModel constant;

  private Date startTime;

  private String displayValue;

  @Id
  @Column(name = "constantvalue_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @OneToOne
  @JoinColumn(name = "constant_id", nullable = false)
  public ConstantModel getConstant() {
    return constant;
  }

  public void setConstant(ConstantModel constant) {
    this.constant = constant;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "constantvalue_datebeg", nullable = false)
  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  @Column(name = "constantvalue_display", nullable = false, columnDefinition = "CHAR(255)", length = 255)
  public String getDisplayValue() {
    return displayValue;
  }

  public void setDisplayValue(String displayValue) {
    this.displayValue = displayValue;
  }

}

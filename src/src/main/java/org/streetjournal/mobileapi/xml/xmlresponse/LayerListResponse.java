package org.streetjournal.mobileapi.xml.xmlresponse;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Список регионов проблем
 */
@XmlRootElement(name = "Response")
public class LayerListResponse extends AbstractResponse {

  private List<LayerData> regions;

  public LayerListResponse() {
  }

  public LayerListResponse(List<LayerData> regions) {
    super(0, "List of layers and categories");
    this.regions = regions;
  }

  @XmlElementRef
  public List<LayerData> getRegions() {
    return regions;
  }

}

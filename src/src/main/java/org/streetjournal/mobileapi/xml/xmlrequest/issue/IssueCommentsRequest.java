package org.streetjournal.mobileapi.xml.xmlrequest.issue;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Зарос списка комментариев к проблеме.
 */
@XmlRootElement(name = "Request")
public class IssueCommentsRequest extends AbstractRequest {

    private Integer issueId;

    public IssueCommentsRequest() {
    }

    public IssueCommentsRequest(Integer issueId) {
        this.issueId = issueId;
    }

    @XmlElement(name = "IssueId")
    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

}

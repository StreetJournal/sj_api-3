package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Ответ
 */
@XmlRootElement(name = "Response")
public class IssueVoteResponse extends AbstractResponse {

    public IssueVoteResponse() {
        this(0, "");
    }

    public IssueVoteResponse(int statusCode, String note) {
        super(statusCode, note);
    }

}

package biz.gelicon.server.service;

import biz.gelicon.server.api.Managed;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * Абстрактный класс-шаблон для конвертора объекта базы данныз в класс данных для передачи
 */
@Managed
public abstract class DataPairBuilder<K, E, P, D> {

  /**
   * Отображение идентификатора -> объект данных для кэширования
   */
  private Map<K, D> map;

  @Nullable
  public final D create(@Nullable E entity, @Nullable P additionalEntity) {

    if (entity == null) {
      return null;
    }

    if (map == null) {
      map = new HashMap<K, D>();
    }

    K entityId = getEntityId(entity);

    D result = map.get(entityId);

    if (result != null) {
      return result;
    }

    map.put(entityId, result = getData(entity, additionalEntity));

    return result;

  }

  /**
   * Извлечение идентификатора из объекта баз данных
   *
   * @param entity
   * @return
   */
  @NotNull
  protected abstract K getEntityId(@NotNull E entity);

  /**
   * Создание объекта данных для передачи из основного и дополнительного объекта данных.
   *
   * @param entity, не равно null
   * @param additionalEntity, может быть null
   * @return
   */
  @NotNull
  protected abstract D getData(@NotNull E entity, @Nullable P additionalEntity);

}

package org.streetjournal.mobileapi.xml.xmlresponse;

import org.streetjournal.mobileapi.xml.BlobAdapter;
import org.streetjournal.mobileapi.xml.Localized;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Category")
public class CategoryData {

    private String code;

    private String name;

    private List<Localized> names = new ArrayList<>();

    private String note;

    private List<String> categoryCase = new ArrayList<>();

    private byte[] icon;

    @XmlAttribute(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlElement(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "LocalizedName")
    public List<Localized> getNames() {
        return names;
    }

    public void setNames(List<Localized> names) {
        this.names = names;
    }

    @XmlElement(name = "Case")
    public List<String> getCategoryCase() {
        return categoryCase;
    }

    public void setCategoryCase(List<String> categoryCase) {
        this.categoryCase = categoryCase;
    }

    @XmlJavaTypeAdapter(BlobAdapter.class)
    @XmlElement(name = "Icon")
    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    @XmlElement(name = "Note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}

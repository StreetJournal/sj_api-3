package org.streetjournal.mobileapi.xml.xmlrequest.object;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Зарос списка комментариев к объекту.
 */
@XmlRootElement(name = "Request")
public class ObjectCommentsRequest extends AbstractRequest {

    private Integer objectId;

    public ObjectCommentsRequest() {
    }

    public ObjectCommentsRequest(Integer objectId) {
        this.objectId = objectId;
    }

    @XmlElement(name = "ObjectId")
    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

}

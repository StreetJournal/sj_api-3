package biz.gelicon.sjua.model;

import javax.persistence.*;

@Entity(name = "AttributeIssue")
@Table(name = "ATTRIBUTEISSUEVAL")
@SequenceGenerator(name="AttributeIssueValIdGen", sequenceName = "attributeissueval_id_gen")
public class AttributeIssue {
    private Integer id;

    private IssueModel issue;

    private String value;

    private Attribute attribute;

    @Id
    @GeneratedValue(generator="AttributeIssueValIdGen")
    @Column(name = "attributeissueval_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "ATTRIBUTEISSUEVAL_VALUE")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "issue_id")
    public IssueModel getIssue() {
        return issue;
    }

    public void setIssue(IssueModel issue) {
        this.issue = issue;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ATTRIBUTE_ID")
    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }
}

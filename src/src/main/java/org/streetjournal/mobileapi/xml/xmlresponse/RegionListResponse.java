package org.streetjournal.mobileapi.xml.xmlresponse;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Список регионов проблем
 */
@XmlRootElement(name = "Response")
public class RegionListResponse extends AbstractResponse {

  private List<RegionData> regions;

  public RegionListResponse() {
  }

  public RegionListResponse(List<RegionData> regions) {
    super(0, "List of regions");
    this.regions = regions;
  }

  @XmlElementRef
  public List<RegionData> getRegions() {
    return regions;
  }

}

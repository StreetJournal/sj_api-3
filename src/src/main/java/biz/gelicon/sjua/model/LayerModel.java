package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.Map;

/**
 * Слои (категории) проблем. Используются для привязки проблем и организаций.
 */
@Entity(name = "Layer")
@Table(name = "layer")
@SequenceGenerator(name = "LayerIdGen", sequenceName = "layer_id_gen")
public class LayerModel {

    public static final int STATUS_INVISIBLE = 0;

    public static final int STATUS_VISIBLE = 1;

    private Integer id;

    private LayerModel parent;

    private LayerTypeModel type;

    private String code;

    private String name;

    private Integer status;

    private Integer order;

    private String note;

    private byte[] iconData;

    private Map<MessageLocaleModel, LayerNameModel> names = new HashMap<>();

    @Id
    @GeneratedValue(generator = "LayerIdGen")
    @Column(name = "layer_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Слой (категория) верхнего уровня, в который входит текущий слой (категория).
     * Корневой слой имеет parent == id
     *
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "ownerlayer_id", nullable = false)
    public LayerModel getParent() {
        return parent;
    }

    public void setParent(LayerModel parent) {
        this.parent = parent;
    }

    /**
     * Тип слоя/категории
     *
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "layertype_id", nullable = false)
    public LayerTypeModel getType() {
        return type;
    }

    public void setType(LayerTypeModel type) {
        this.type = type;
    }

    /**
     * Код слоя/категории
     *
     * @return
     */
    @Column(name = "layer_code", nullable = true)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Название слоя/категории
     *
     * @return
     */
    @Column(name = "layer_name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Статус-признак видимости слоя/категории.
     *
     * @return
     */
    @Column(name = "layer_status", nullable = false)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Порядок вывода слоя/категории
     *
     * @return
     */
    @Column(name = "layer_order", nullable = false)
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    /**
     * Примечание слоя/категории
     *
     * @return
     */
    @Column(name = "layer_remark", nullable = true)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    /**
     * Картинки в формате PNG
     *
     * @return
     */
    @Column(name = "layer_image", nullable = true)
    public byte[] getIconData() {
        return iconData;
    }

    public void setIconData(byte[] iconData) {
        this.iconData = iconData;
    }

    @OneToMany(mappedBy="layer")
    @MapKey(name="locale")
    public Map<MessageLocaleModel, LayerNameModel> getNames() {
        return names;
    }

    public void setNames(Map<MessageLocaleModel, LayerNameModel> names) {
        this.names = names;
    }
}

package biz.gelicon.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

public class EntityManagerHolder {

  private static final Logger logger = LoggerFactory.getLogger(EntityManagerHolder.class);

  private EntityManager em;

  public EntityManager getEntityManager() {
    return em;
  }

  public void setEntityManager(EntityManager em) {
    this.em = em;
  }

  public void close() {
    if (em != null) {
      logger.info("Close assigned EntityManager");
      try {
        em.close();
      } finally {
        em = null;
      }
    }
  }

}

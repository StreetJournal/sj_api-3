package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Тип слоя
 */
@Entity(name = "LayerType")
@Table(name = "layertype")
@SequenceGenerator(name="LayerTypeIdGen", sequenceName = "layertype_id_gen")
public class LayerTypeModel {

  private Integer id;

  private String name;

  @Id
  @GeneratedValue(generator="LayerTypeIdGen")
  @Column(name = "layertype_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "layertype_name", nullable = false, length = 100)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}

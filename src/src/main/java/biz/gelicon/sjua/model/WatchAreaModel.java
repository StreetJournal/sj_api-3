package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;


//
///*==============================================================*/
///* Table: watcharea                                             */
///*==============================================================*/
//create table watcharea (
//        watcharea_id         INTEGER                        not null,
//        proguser_id          INTEGER                        not null,
//        toponym_id           INTEGER,
//        communicationtype_id INTEGER                        not null,
//        organizationtype_id  INTEGER                        not null,
//        watcharea_name       VARCHAR(255)                   not null,
//        watcharea_description BLOB,
//        watcharea_email      BLOB,
//        watcharea_newissue   INTEGER                        not null,
//        watcharea_newcomment INTEGER                        not null,
//        watcharea_public     INTEGER                        not null,
//        watcharea_date       DATE                           not null,
//        watcharea_datemodify DATE                           not null,
//        watchareaforeign_id  VARCHAR(255),
//        watcharea_remark     VARCHAR(255),
//        watcharea_chief      VARCHAR(255),
//        watcharea_address    VARCHAR(255),
//        watcharea_phone      VARCHAR(255),
//        watcharea_proflag    INTEGER                        not null,
//        watcharea_chiefpos   VARCHAR(255),
//        watcharea_web        VARCHAR(255),
//        watcharea_logo       BLOB,
//        watcharea_source     VARCHAR(255),
//        watcharea_chief2     VARCHAR(255),
//        watcharea_chiefpos2  VARCHAR(255),

/**
 * Тип организации
 */
@Entity(name = "WatchArea")
@Table(name = "watcharea")
@SequenceGenerator(name = "WatchAreaIdGen", sequenceName = "watcharea_id_gen")
public class WatchAreaModel {

  private Integer id;

  private UserModel user;

  private ToponymModel toponym;

  private CommunicationTypeModel communicationType;

  private OrganizationTypeModel type;

  private String name;

  private String description;

  private String email;

  private Boolean newIssuesSubscribe;

  private Boolean newCommentsSubscribe;

  private Integer publicCode;

  private Date creationDate;

  private Date modificationDate;

  private String foreignCode;

  private String note;

  private String chief;

  private String chiefTreatmentForm;

  private String chiefPosition;

  private String chiefPositionTreatmentForm;

  private String address;

  private String phone;

  private String site;

  private Boolean proAccount;

  private byte[] logo;

  private String source;

  @Id
  @GeneratedValue(generator = "WatchAreaIdGen")
  @Column(name = "watcharea_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Пользователь
   *
   * @return
   */
  @ManyToOne
  @JoinColumn(name = "proguser_id", nullable = false)
  public UserModel getUser() {
    return user;
  }

  public void setUser(UserModel user) {
    this.user = user;
  }

  /**
   * Топоним
   *
   * @return
   */
  @ManyToOne
  @JoinColumn(name = "toponym_id", nullable = true)
  public ToponymModel getToponym() {
    return toponym;
  }

  public void setToponym(ToponymModel toponym) {
    this.toponym = toponym;
  }

  /**
   * Канал взаимодействия
   *
   * @return
   */
  @ManyToOne
  @JoinColumn(name = "communicationtype_id", nullable = false)
  public CommunicationTypeModel getCommunicationType() {
    return communicationType;
  }

  public void setCommunicationType(CommunicationTypeModel communicationType) {
    this.communicationType = communicationType;
  }

  /**
   * Тип организации
   *
   * @return
   */
  @ManyToOne
  @JoinColumn(name = "organizationtype_id", nullable = false)
  public OrganizationTypeModel getType() {
    return type;
  }

  public void setType(OrganizationTypeModel type) {
    this.type = type;
  }

  /**
   * Наименование
   *
   * @return
   */
  @Column(name = "watcharea_name", nullable = false, length = 255)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  /**
   * Описание
   *
   * @return
   */
  @Lob
  @Column(name = "watcharea_description", nullable = true)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Электронная почта
   *
   * @return
   */
  @Lob
  @Column(name = "watcharea_email", nullable = true)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * Подписка на новые проблемы
   *
   * @return
   */
  @Column(name = "watcharea_newissue", columnDefinition = "INTEGER", nullable = false)
  public Boolean getNewIssuesSubscribe() {
    return newIssuesSubscribe;
  }

  public void setNewIssuesSubscribe(Boolean newIssuesSubscribe) {
    this.newIssuesSubscribe = newIssuesSubscribe;
  }

  /**
   * Подписка на новые комментарии
   *
   * @return
   */
  @Column(name = "watcharea_newcomment", columnDefinition = "INTEGER", nullable = false)
  public Boolean getNewCommentsSubscribe() {
    return newCommentsSubscribe;
  }

  public void setNewCommentsSubscribe(Boolean newCommentsSubscribe) {
    this.newCommentsSubscribe = newCommentsSubscribe;
  }

  /**
   * Флаг публичной области: 0, 1, -1
   *
   * @return
   */
  @Column(name = "watcharea_public", nullable = false)
  public Integer getPublicCode() {
    return publicCode;
  }

  public void setPublicCode(Integer publicCode) {
    this.publicCode = publicCode;
  }

  /**
   * Дата создания
   *
   * @return
   */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "watcharea_date", nullable = false)
  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  /**
   * Дата модификации
   *
   * @return
   */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "watcharea_datemodify", nullable = false)
  public Date getModificationDate() {
    return modificationDate;
  }

  public void setModificationDate(Date modificationDate) {
    this.modificationDate = modificationDate;
  }

  /**
   * Идентификатор во внешней системе
   *
   * @return
   */
  @Column(name = "watchareaforeign_id", length = 255, nullable = true)
  public String getForeignCode() {
    return foreignCode;
  }

  public void setForeignCode(String foreignCode) {
    this.foreignCode = foreignCode;
  }

  /**
   * Примечание
   *
   * @return
   */
  @Column(name = "watcharea_remark", length = 255, nullable = true)
  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  /**
   * ФИО руководителя
   *
   * @return
   */
  @Column(name = "watcharea_chief", length = 255, nullable = true)
  public String getChief() {
    return chief;
  }

  public void setChief(String chief) {
    this.chief = chief;
  }

  /**
   * ФИО руководителя в нужном падеже
   *
   * @return
   */
  @Column(name = "watcharea_chief2", length = 255, nullable = true)
  public String getChiefTreatmentForm() {
    return chiefTreatmentForm;
  }

  public void setChiefTreatmentForm(String chiefTreatmentForm) {
    this.chiefTreatmentForm = chiefTreatmentForm;
  }

  /**
   * Должность руководителя
   *
   * @return
   */
  @Column(name = "watcharea_chiefpos", length = 255, nullable = true)
  public String getChiefPosition() {
    return chiefPosition;
  }

  public void setChiefPosition(String chiefPosition) {
    this.chiefPosition = chiefPosition;
  }

  /**
   * Должность руководителя в нужном падеже
   *
   * @return
   */
  @Column(name = "watcharea_chiefpos2", length = 255, nullable = true)
  public String getChiefPositionTreatmentForm() {
    return chiefPositionTreatmentForm;
  }

  public void setChiefPositionTreatmentForm(String chiefPositionTreatmentForm) {
    this.chiefPositionTreatmentForm = chiefPositionTreatmentForm;
  }

  /**
   * Адрес
   *
   * @return
   */
  @Column(name = "watcharea_address", length = 255, nullable = true)
  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * Телефон
   *
   * @return
   */
  @Column(name = "watcharea_phone", length = 255, nullable = true)
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  /**
   * Web сайт организации
   *
   * @return
   */
  @Column(name = "watcharea_web", length = 255, nullable = true)
  public String getSite() {
    return site;
  }

  public void setSite(String site) {
    this.site = site;
  }

  /**
   * Флаг PRO аккаунта
   *
   * @return
   */
  @Column(name = "watcharea_proflag", columnDefinition = "INTEGER", nullable = false)
  public Boolean getProAccount() {
    return proAccount;
  }

  public void setProAccount(Boolean proAccount) {
    this.proAccount = proAccount;
  }

  /**
   * Логотип
   *
   * @return
   */
  @Lob
  @Column(name = "watcharea_logo", nullable = true)
  public byte[] getLogo() {
    return logo;
  }

  public void setLogo(byte[] logo) {
    this.logo = logo;
  }

  /**
   * Источник информации
   *
   * @return
   */
  @Column(name = "watcharea_source", length = 255, nullable = true)
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

}

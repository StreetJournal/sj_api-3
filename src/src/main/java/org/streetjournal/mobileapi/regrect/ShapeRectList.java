package org.streetjournal.mobileapi.regrect;

import org.streetjournal.mobileapi.xml.Bound;
import org.streetjournal.mobileapi.xml.BoundAdapter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Bounds")
public class ShapeRectList {

    @XmlRootElement(name = "Bound")
    public static class RegionBound {

        @XmlAttribute(name = "code")
        public String code;

        @XmlJavaTypeAdapter(BoundAdapter.class)
        @XmlAttribute(name = "swne")
        public Bound bound;

        public RegionBound() {
        }

        public RegionBound(String code, int south, int west, int north, int east) {
            this.code = code;
            bound = new Bound(south, west, north, east);
        }

    }

    @XmlElementRef
    public List<RegionBound> bounds = new ArrayList<>();

}

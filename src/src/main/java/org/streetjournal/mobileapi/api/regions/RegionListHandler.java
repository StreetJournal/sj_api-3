package org.streetjournal.mobileapi.api.regions;

import biz.gelicon.sjua.model.ToponymModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.RegionListRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.RegionData;
import org.streetjournal.mobileapi.xml.xmlresponse.RegionListResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@javax.ws.rs.Path("get_region_list")
public class RegionListHandler {

  private static final Logger logger = LoggerFactory.getLogger(RegionListHandler.class);

  @Inject
  EntityManager em;

  private final List<RegionData> result = new ArrayList<>();

  private RegionListRequest request;

  @POST
  @Consumes(MediaType.APPLICATION_XML)
  @Produces(MediaType.APPLICATION_XML)
  public AbstractResponse execute(RegionListRequest request) {

    this.request = request;

    prepareResult();

    return new RegionListResponse(result);

  }

  private void prepareResult() {

    for (ToponymModel item : em.createQuery("SELECT e FROM Toponym e WHERE e.parent IS NOT NULL and e.type <> 0",
            ToponymModel.class).getResultList()) {

      if (item.getCountry() == null || item.getType().equals(1)) {
        continue;
      }

      RegionData data = new RegionData();

      String country = item.getCountry().getCode();

//      data.setCountry(item.getCountry().getCode());

      if (item.getParent() != null && !item.getParent().getCode().equals(item.getCode())) {
        data.setParent(country + item.getParent().getCode());
      }

      data.setCode(country + item.getCode());

      data.setName(item.getName());

      result.add(data);

    }

  }

}

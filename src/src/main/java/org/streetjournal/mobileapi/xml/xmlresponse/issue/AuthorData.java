package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 */
@XmlRootElement(name = "Author")
public class AuthorData {

    private Integer id;

    private String name;

    public AuthorData() {
    }

    public AuthorData(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @XmlAttribute(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlValue
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

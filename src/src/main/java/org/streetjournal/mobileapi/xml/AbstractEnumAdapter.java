package org.streetjournal.mobileapi.xml;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 */
public class AbstractEnumAdapter<E extends Enum<E>> extends XmlAdapter<String, E> {

  private final Class<E> type;

  public AbstractEnumAdapter(Class<E> type) {
    this.type = type;
  }

  @Override
  public E unmarshal(String v) throws Exception {
    return Enum.valueOf(type, v.toUpperCase());
  }

  @Override
  public String marshal(E v) throws Exception {
    return v.name().toLowerCase();
  }

}

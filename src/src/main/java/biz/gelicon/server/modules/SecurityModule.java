package biz.gelicon.server.modules;

import biz.gelicon.server.api.UserIdValue;
import biz.gelicon.server.config.TicketConfig;
import biz.gelicon.server.service.TicketControl;
import biz.gelicon.server.service.UserIdValueHolder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 */
public class SecurityModule extends AbstractModule {

  private static final Logger logger = LoggerFactory.getLogger(SecurityModule.class);

  /**
   * Алгоритм, используемый для хэширования секрета из конфигурации
   */
  private static final String SECRET_HASH_ALGORITHM = "SHA-256";

  private final Lock ticketHashLock = new ReentrantLock();

  /**
   * Хэш секрета из конфигурации, используемый, как ключ тикетов
   */
  private byte[] ticketHash;

  @Override
  protected void configure() {
  }

  @Provides
  @UserIdValue
  public Integer getUserId(UserIdValueHolder userIdValueHolder) {
    return userIdValueHolder.getUserId();
  }

  @Provides
  public TicketControl getTicketControl(TicketConfig ticketConfig) {

    byte[] ticketHash;

    ticketHashLock.lock();

    try {
      ticketHash = this.ticketHash;

      if (ticketHash == null) {

        MessageDigest md;

        try {
          md = MessageDigest.getInstance(SECRET_HASH_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
          throw new RuntimeException(e);
        }

        try {
          this.ticketHash = ticketHash = Arrays
                  .copyOf(md.digest(ticketConfig.secret.getBytes("UTF-8")), 16);
        } catch (UnsupportedEncodingException e) {
          throw new RuntimeException(e);
        }

      }

    } finally {
      ticketHashLock.unlock();
    }

    return new TicketControl(ticketHash);

  }

}

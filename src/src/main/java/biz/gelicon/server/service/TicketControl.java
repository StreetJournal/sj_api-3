package biz.gelicon.server.service;

import java.nio.ByteBuffer;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

public class TicketControl {

  private static final String CIPHER = "AES/ECB/NoPadding";

  private static final String KEY_ALG = "AES";

  private final byte[] key;

  private long timestamp;

  private long value;

  private byte[] ticket;

  public TicketControl(byte[] key) {
    this.key = key;
  }

  public void setTicket(long timestamp, int lo, int hi) {
    setTicket(timestamp, lo | (((long) hi) << 32));
  }

  public void setTicket(long timestamp, long value) {

    try {
      Cipher cipher = Cipher.getInstance(CIPHER);

      cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, KEY_ALG));

      ByteBuffer data = ByteBuffer.allocate(32);

      /*
       * X -> -X -X -> X Y -> -Y -Y -> Y
       * 
       * X -> Y Y -> -X Y -> -X -X -> -Y
       */

      data.putLong(0, timestamp);
      data.putLong(8, value);
      data.putLong(16, value);
      data.putLong(24, -timestamp);

      ticket = cipher.doFinal(data.array());

      this.timestamp = timestamp;
      this.value = value;

    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public void setTicket(byte[] ticket) {

    if (ticket.length != 32) {
      throw new IllegalArgumentException("Ticket size must be 32");
    }

    try {

      Cipher cipher = Cipher.getInstance(CIPHER);

      cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, KEY_ALG));

      ByteBuffer data = ByteBuffer.wrap(cipher.doFinal(ticket));

      long timestamp = data.getLong(0);
      long value = data.getLong(8);

      if (timestamp != -data.getLong(24) || value != data.getLong(16)) {
        throw new IllegalArgumentException("Ticket verification error");
      }

      this.ticket = ticket;

      this.timestamp = timestamp;
      this.value = value;

    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

  }

  public byte[] getTicket() {
    return ticket;
  }

  public void setBase64Ticket(String ticket) {
    setTicket(Base64.decodeBase64(ticket));
  }

  public void setHexTicket(String ticket) {
    try {
      setTicket(Hex.decodeHex(ticket.toCharArray()));
    } catch (DecoderException e) {
      throw new RuntimeException(e);
    }
  }

  public String getBase64Ticket() {
    return Base64.encodeBase64String(ticket);
  }

  public String getHexTicket() {
    return Hex.encodeHexString(ticket);
  }

  public long getTimestamp() {
    return timestamp;
  }

  public long getValue() {
    return value;
  }

  public int getIntValue() {
    return (int) (value & 0xffffffffL);
  }
  
}

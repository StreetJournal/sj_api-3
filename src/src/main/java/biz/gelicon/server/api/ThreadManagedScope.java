package biz.gelicon.server.api;

import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ThreadManagedScope implements Scope {

  private static final Logger logger = LoggerFactory.getLogger(ThreadManagedScope.class);

  private final ThreadLocal<Map<Provider<?>, Object>> managed =
          new ThreadLocal<Map<Provider<?>, Object>>();

  @Override
  public <T> Provider<T> scope(Key<T> key, final Provider<T> unscoped) {

    return new Provider<T>() {

      @SuppressWarnings("unchecked")
      public T get() {
        return (T) getProviderValue(unscoped);
      }

    };

  }

  public void enter() {

    logger.info("Managed scope enter");

    if (managed.get() != null) {
      throw new RuntimeException(String.format("Active scope already exists for thread=[%s]",
              Thread.currentThread()));
    }

    managed.set(new HashMap<Provider<?>, Object>());

  }

  public void exit() {

    Map<Provider<?>, Object> map = managed.get();

    if (map == null) {
      throw new RuntimeException(String.format("Scope is not active for thread=[%s]", Thread
              .currentThread()));
    }

    int count = 0;

    for (Object value : map.values()) {

      count++;

      try {
        close(value);
      } catch (Exception e) {
        logger.error("Error on close [{}]", value.getClass().getName());
      }

    }

    logger.info("Managed scope exit, closed count={}", count);

    managed.set(null);

  }

  private Object getProviderValue(Provider<?> provider) {

    Map<Provider<?>, Object> map = managed.get();

    if (map == null) {
      throw new RuntimeException(String.format("Scope is not active for thread=[%s]", Thread
              .currentThread()));
    }

    Object value = map.get(provider);

    if (value == null) {

      value = provider.get();

      if (value != null) {
        map.put(provider, value);
      }

    }

    return value;

  }

  protected void close(Object object) throws Exception {

    try {

      Method m = object.getClass().getMethod("close", (Class<?>[]) null);

      m.invoke(object, (Object[]) null);

    } catch (NoSuchMethodException e) {
      logger.info("Class {} has no close() method", object.getClass().getName());
    }

  }

}

package org.streetjournal.mobileapi.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 */
@XmlRootElement(name = "Shapes")
public class ShapeFileConfig {

    @XmlElement(name="RegionBoundsFile")
    public String regionBoundsFile;

}

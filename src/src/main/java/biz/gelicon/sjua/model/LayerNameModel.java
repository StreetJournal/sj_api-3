package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Локализованное имя категории
 */
@Entity(name = "LayerName")
@Table(name = "layerlocale")
@SequenceGenerator(name = "LayerNameIdGen", sequenceName = "layerlocale_id_gen")
public class LayerNameModel {

    private Integer id;

    private String name;

    private MessageLocaleModel locale;

    private LayerModel layer;

    @Id
    @GeneratedValue(generator = "LayerNameIdGen")
    @Column(name = "layerlocale_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "layerlocale_name", nullable = false, length = 200)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "messagelocale_id", nullable = false)
    public MessageLocaleModel getLocale() {
        return locale;
    }

    public void setLocale(MessageLocaleModel locale) {
        this.locale = locale;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "layer_id", nullable = false)
    public LayerModel getLayer() {
        return layer;
    }

    public void setLayer(LayerModel layer) {
        this.layer = layer;
    }

}

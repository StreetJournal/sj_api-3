package org.streetjournal.mobileapi.xml;

import org.streetjournal.mobileapi.xml.xmlresponse.LocationData;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 */
public class LocationAdapter extends XmlAdapter<String, LocationData> {

  @Override
  public LocationData unmarshal(String value) throws Exception {

    String[] parts = value.split(",");

    if (parts.length != 2) {
      throw new RuntimeException("Bad syntax for LocationData [" + value + "]");
    }

    return new LocationData(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));

  }

  @Override
  public String marshal(LocationData value) throws Exception {

//    if(value == null) {
//      return null;
//    }

    return value.getLat() + "," + value.getLng();

  }

}

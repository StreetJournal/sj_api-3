package org.streetjournal.mobileapi.xml.xmlrequest.issue;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Зарос списка событий к проблеме.
 */
@XmlRootElement(name = "Request")
public class IssueEventsRequest extends AbstractRequest {

    private Integer issueId;

    public IssueEventsRequest() {
    }

    public IssueEventsRequest(Integer issueId) {
        this.issueId = issueId;
    }

    @XmlElement(name = "IssueId")
    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

}

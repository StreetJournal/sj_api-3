package org.streetjournal.mobileapi.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.format.IllegalDateEncodingException;
import org.streetjournal.mobileapi.xml.NoDataResponse;

import javax.inject.Singleton;
import javax.persistence.NoResultException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 */
@Provider
@Singleton
public class HandlerExceptionMapper implements ExceptionMapper<Throwable> {

    private static final Logger logger = LoggerFactory.getLogger(HandlerExceptionMapper.class);

    public HandlerExceptionMapper() {
        logger.info("Create");
    }

    @Override
    public Response toResponse(Throwable e) {

        logger.error("WebApplicationException", e);

        Throwable cause = e.getCause();

        if (cause instanceof javax.xml.bind.UnmarshalException) {
            return Response.ok(new NoDataResponse(-1, "parse_error",
                    "XML parse error")).status(400).build();
        }

        if (e instanceof IllegalDateEncodingException) {
            return Response.ok(new NoDataResponse(-1, "invalid_datetime_value",
                    "Invalid datetime value")).status(400).build();
        }

        if (e instanceof com.sun.jersey.api.NotFoundException) {
            return Response.ok(new NoDataResponse(-1, "unknown_access_address",
                    "Unknown access address")).status(400).build();
        }

        if (e instanceof NoResultException) {
            return Response.ok(new NoDataResponse(-1, "incorrect_entity_id"), "No entity found for requested id")
                    .status(404).build();
        }

        return Response.ok(new NoDataResponse(-1, "unexpected_error",
                e.getMessage())).status(500).build();

    }

}

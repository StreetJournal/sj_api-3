package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.Set;

/**
 *
 */
@Entity(name = "Issue")
@Table(name = "issue")
@SequenceGenerator(name = "IssueIdGen", sequenceName = "issue_id_gen")
public class IssueModel {

    /**
     * Не определено
     */
    public static final int CANEMAIL_UNDEFINED = 0;

    /**
     * Опубликовано
     */
    public static final int CANEMAIL_PUBLISHED = 1;

    /**
     * Спам
     */
    public static final int CANEMAIL_SPAM = -1;

    /**
     * Для своих
     */
    public static final int CANEMAIL_PRIVATE = -2;

    private Integer id;

    private IssueStatusModel status;

    private Double lalitude;

    private Double longitude;

    private String name;

    private String address;

    private AddressGroupModel addressGroup;

    private ToponymModel toponym;

    private Integer viewCount;

    private Integer mailMode;

    private String note;

    private MapTypeModel mapType;

    private Date beginDate;

    private Date endDate;

    private PointIconModel icon;

    private Date moderationDate;

    private Date changeStatusDate;

    private Set<LayerModel> layers;

    @Id
    @GeneratedValue(generator = "IssueIdGen")
    @Column(name = "issue_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "issuestatus_id", nullable = false)
    public IssueStatusModel getStatus() {
        return status;
    }

    public void setStatus(IssueStatusModel status) {
        this.status = status;
    }

    @Column(name = "issue_latitude")
    public Double getLalitude() {
        return lalitude;
    }

    public void setLalitude(Double lalitude) {
        this.lalitude = lalitude;
    }

    @Column(name = "issue_longitude")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Column(name = "issue_name", length = 255, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "issue_address", length = 255, nullable = false)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressgroup_id", nullable = true)
    public AddressGroupModel getAddressGroup() {
        return addressGroup;
    }

    public void setAddressGroup(AddressGroupModel addressGroup) {
        this.addressGroup = addressGroup;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "toponym_id", nullable = true)
    public ToponymModel getToponym() {
        return toponym;
    }

    public void setToponym(ToponymModel toponym) {
        this.toponym = toponym;
    }

    @Column(name = "issue_viewcount", nullable = false)
    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    /**
     * -2, -1, 0, 1
     *
     * @return
     */
    @Column(name = "issue_canemail", nullable = false)
    public Integer getMailMode() {
        return mailMode;
    }

    public void setMailMode(Integer mailMode) {
        this.mailMode = mailMode;
    }

    @Column(name = "issue_remark", length = 255, nullable = true)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "maptype_id", nullable = false)
    public MapTypeModel getMapType() {
        return mapType;
    }

    public void setMapType(MapTypeModel mapType) {
        this.mapType = mapType;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "issue_datebeg", nullable = false)
    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "issue_dateend", nullable = false)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @ManyToOne
    @JoinColumn(name = "pointimage_id", nullable = true)
    public PointIconModel getIcon() {
        return icon;
    }

    public void setIcon(PointIconModel icon) {
        this.icon = icon;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "issue_datemoderate", nullable = false)
    public Date getModerationDate() {
        return moderationDate;
    }

    public void setModerationDate(Date moderationDate) {
        this.moderationDate = moderationDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "issue_changestatus", nullable = true)
    public Date getChangeStatusDate() {
        return changeStatusDate;
    }

    public void setChangeStatusDate(Date changeStatusDate) {
        this.changeStatusDate = changeStatusDate;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "issuelayer",
        joinColumns = {@JoinColumn(name = "issue_id", referencedColumnName = "issue_id")},
        inverseJoinColumns = {@JoinColumn(name = "layer_id", referencedColumnName = "layer_id")}
    )
    public Set<LayerModel> getLayers() {
        return layers;
    }

    public void setLayers(Set<LayerModel> layers) {
        this.layers = layers;
    }

}

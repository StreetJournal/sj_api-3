package org.streetjournal.mobileapi.xml;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.IllegalFieldValueException;
import org.streetjournal.mobileapi.format.IllegalDateEncodingException;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Date;
import java.util.Locale;

/**
 *
 */
public class BlobAdapter extends XmlAdapter<String, byte[]> {

  @Override
  public byte[] unmarshal(String value) throws Exception {
    return Base64.decodeBase64(value);
  }

  @Override
  public String marshal(byte[] value) throws Exception {
    return Base64.encodeBase64String(value);
  }

}

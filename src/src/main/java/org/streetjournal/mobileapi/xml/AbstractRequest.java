package org.streetjournal.mobileapi.xml;

import javax.xml.bind.annotation.XmlTransient;

/**
 * Суперкласс для классов запросов
 */
@XmlTransient
public class AbstractRequest {
}

package org.streetjournal.mobileapi.xml.xmlresponse.object;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Response")
public class ObjectDataResponse extends AbstractResponse {

    private List<ObjectData> object;

    public ObjectDataResponse() {
    }

    public ObjectDataResponse(List<ObjectData> object) {
        super(0, "Data found");
        this.object = object;
    }

    @XmlElementRef
    public List<ObjectData> getIssues() {
        return object;
    }

}

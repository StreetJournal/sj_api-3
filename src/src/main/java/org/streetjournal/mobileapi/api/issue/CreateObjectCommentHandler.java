package org.streetjournal.mobileapi.api.issue;

import biz.gelicon.server.service.TicketControl;
import biz.gelicon.sjua.model.IssueCommentModel;
import biz.gelicon.sjua.model.IssueCommentTypeModel;
import biz.gelicon.sjua.model.IssueModel;
import biz.gelicon.sjua.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.issue.CreateObjectCommentRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.CreateObjectCommentResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Date;

/**
 *
 */
@Path("create_object_comment")
public class CreateObjectCommentHandler {

    private static final Logger logger = LoggerFactory.getLogger(CreateObjectCommentHandler.class);

    @Inject
    EntityManager em;

    @Inject
    TicketControl ticketControl;

    private CreateObjectCommentRequest request;

    private Integer objectCommentId;

    private Integer userId;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public AbstractResponse execute(CreateObjectCommentRequest request) {

        this.request = request;

        authentificate();

        em.getTransaction().begin();

        createIssueComment();

        em.getTransaction().commit();

        return new CreateObjectCommentResponse(objectCommentId);

    }

    private void authentificate() {
        ticketControl.setHexTicket(request.getToken());
        userId = ticketControl.getIntValue();
    }

    private void createIssueComment() {

        Date now = new Date();

        IssueModel issue = em.find(IssueModel.class, request.getObjectId());

        IssueCommentModel replyToComment;

        if (request.getReplyToId() != null) {

            replyToComment = em.find(IssueCommentModel.class, request.getReplyToId());

            if (replyToComment.getIssue() != issue) {
                throw new RuntimeException("Mismatch reply to comment and object");
            }

        } else {

            replyToComment = em.createQuery(
                "SELECT e FROM IssueComment e " +
                    "WHERE e.issue.id=:issueId AND e.replyTo IS NULL", IssueCommentModel.class
            )
                .setParameter("issueId", request.getObjectId())
                .getSingleResult();

        }

        IssueCommentModel issueComment = new IssueCommentModel();

        issueComment.setIssue(issue);
        issueComment.setReplyTo(replyToComment);
        issueComment.setUser(em.getReference(UserModel.class, userId));
        issueComment.setType(em.getReference(IssueCommentTypeModel.class, IssueCommentTypeModel.TYPE_COMMENT));
        issueComment.setAnonymous(request.getAnonymous() != null && request.getAnonymous());
        issueComment.setDeleteStatus(IssueCommentModel.STATUS_NORMAL);
        issueComment.setText(request.getText());

        issueComment.setCreationDate(now);
        issueComment.setModificationDate(now);

        em.persist(issueComment);

        objectCommentId = issueComment.getId();
    }

}

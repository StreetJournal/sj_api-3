package org.streetjournal.mobileapi.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Суперкласс для классов ответов
 */
@XmlTransient
public abstract class AbstractResponse {

    /**
     * Код статуса
     */
    @XmlAttribute(name = "status", required = true)
    public int statusCode;

    /**
     * Примечание к коду статуса
     */
    @XmlAttribute(name = "note", required = true)
    public String note;

    /**
     * Примечание к коду статуса
     */
    @XmlAttribute(name = "code")
    public String code;

    public AbstractResponse() {
    }

    public AbstractResponse(int statusCode, String note) {
        this.statusCode = statusCode;
        this.note = note;
        this.code = statusCode == 0 ? "ok" : null;
    }

    public AbstractResponse(int statusCode, String code, String note) {
        this.statusCode = statusCode;
        this.code = code;
        this.note = note;
    }

    public AbstractResponse(String code, String note) {
        this.code = code;
        this.statusCode = "ok".equals(code) ? 0 : 1;
        this.note = note;
    }

}

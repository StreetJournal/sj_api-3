package org.streetjournal.mobileapi.xml.xmlresponse;

import org.apache.commons.lang3.LocaleUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Locale;

public class LocaleAdapter extends XmlAdapter<String, Locale> {
    @Override
    public Locale unmarshal(String v) throws Exception {
        return LocaleUtils.toLocale(v);
    }

    @Override
    public String marshal(Locale v) throws Exception {
        return v.toString();
    }
}

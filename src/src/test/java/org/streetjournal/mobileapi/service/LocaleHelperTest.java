package org.streetjournal.mobileapi.service;

import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class LocaleHelperTest {

    LocaleHelper helper;

    @Before
    public void setUp() throws Exception {
        this.helper = new LocaleHelperImpl();
    }

    @Test
    public void testNormalizeReturnsRussianLanguageForRussianLocaleInRussia() {
        Locale locale = helper.normalize(new Locale("ru", "RU"));

        assertEquals("ru", locale.getLanguage());
    }

    @Test
    public void testNormalizeReturnsRussianLanguageForRussianLocaleInUkraine() {
        Locale locale = helper.normalize(new Locale("ru", "UA"));

        assertEquals("ru", locale.getLanguage());
    }

    @Test
    public void testNormalizeReturnsUkrainianLanguageForUkrainianLocale() {
        Locale locale = helper.normalize(new Locale("uk", "UA"));

        assertEquals("uk", locale.getLanguage());
    }

    @Test
    public void testNormalizeReturnsEnglishLanguageForEnglishLocaleInUSA() {
        Locale locale = helper.normalize(new Locale("en", "US"));

        assertEquals("en", locale.getLanguage());
    }

    @Test
    public void testNormalizeReturnsEnglishLanguageForEnglishLocaleInGB() {
        Locale locale = helper.normalize(new Locale("en", "GB"));

        assertEquals("en", locale.getLanguage());
    }

    @Test
    public void testNormalizeReturnsEnglishLanguageForFrenchLocale() {
        Locale locale = helper.normalize(new Locale("fr", "FR"));

        assertEquals("en", locale.getLanguage());
    }

    @Test
    public void testNormalizeReturnsNullForNullLocale() {
        Locale locale = helper.normalize(null);

        assertEquals(null, locale);
    }

}
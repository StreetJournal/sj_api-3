package org.streetjournal.mobileapi.xml.xmlrequest;

import org.streetjournal.mobileapi.xml.AbstractRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.LocaleAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Locale;

/**
 * Запрос списка регионов
 */
@XmlRootElement(name="Request")
public class RegionListRequest extends AbstractRequest {
    private Locale locale;

    @XmlElement(name = "Locale")
    @XmlJavaTypeAdapter(LocaleAdapter.class)
    public Locale getLocale() {
        return locale;
    }

    public RegionListRequest setLocale(Locale locale) {
        this.locale = locale;
        return this;
    }
}

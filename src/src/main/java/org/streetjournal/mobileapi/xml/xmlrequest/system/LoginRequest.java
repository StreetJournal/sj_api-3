package org.streetjournal.mobileapi.xml.xmlrequest.system;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Данные для запроса на вход
 */
@XmlRootElement(name="Request")
public class LoginRequest extends AbstractRequest {

  private String login;

  private String password;

  public LoginRequest() {
  }

  public LoginRequest(String login, String password) {
    this.login = login;
    this.password = password;
  }

  @XmlElement(name = "Login")
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  @XmlElement(name = "Password")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

}

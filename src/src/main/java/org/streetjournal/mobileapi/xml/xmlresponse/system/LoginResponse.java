package org.streetjournal.mobileapi.xml.xmlresponse.system;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Список регионов проблем
 */
@XmlRootElement(name = "Response")
public class LoginResponse extends AbstractResponse {

    private String token;

    private Long userId;

    private String name;

    public LoginResponse() {
    }

    public LoginResponse(String token, Long userId, String name, String code, int status) {
        super(status, code, "Login success");
        this.token = token;
        this.userId = userId;
        this.name = name;
    }

    @XmlElement(name = "Token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlElement(name = "UserId")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @XmlElement(name = "Name")
    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

}

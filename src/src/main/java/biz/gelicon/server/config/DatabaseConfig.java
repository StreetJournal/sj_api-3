package biz.gelicon.server.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Database")
public class DatabaseConfig {

  @XmlElement(name = "User")
  public String user;

  @XmlElement(name = "Password")
  public String password;

  @XmlElement(name = "URL")
  public String url;

}

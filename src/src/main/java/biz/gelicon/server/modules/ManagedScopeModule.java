package biz.gelicon.server.modules;

import biz.gelicon.server.api.Managed;
import biz.gelicon.server.api.ThreadManagedScope;
import com.google.inject.AbstractModule;

/**
 *
 */
public class ManagedScopeModule extends AbstractModule {

  @Override
  protected void configure() {

    ThreadManagedScope managedScope = new ThreadManagedScope();

    bindScope(Managed.class, managedScope);

    bind(ThreadManagedScope.class).toInstance(managedScope);

  }

}

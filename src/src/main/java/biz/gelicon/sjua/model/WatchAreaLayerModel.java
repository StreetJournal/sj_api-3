package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Модель связи организации/области контроля и слоя/категории
 */
@Entity(name = "WatchAreaLayer")
@Table(name = "watcharealayer")
@SequenceGenerator(name = "WatchAreaLayerIdGen", sequenceName = "watcharealayer_id_gen")
public class WatchAreaLayerModel {

  private Integer id;

  private WatchAreaModel watchArea;

  private LayerModel layer;

  @Id
  @GeneratedValue(generator = "WatchAreaLayerIdGen")
  @Column(name = "watcharealayer_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @ManyToOne
  @JoinColumn(name = "watcharea_id", nullable = false)
  public WatchAreaModel getWatchArea() {
    return watchArea;
  }

  public void setWatchArea(WatchAreaModel watchArea) {
    this.watchArea = watchArea;
  }

  @ManyToOne
  @JoinColumn(name = "layer_id", nullable = false)
  public LayerModel getLayer() {
    return layer;
  }

  public void setLayer(LayerModel layer) {
    this.layer = layer;
  }

}

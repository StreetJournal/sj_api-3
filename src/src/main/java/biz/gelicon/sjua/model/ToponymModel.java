package biz.gelicon.sjua.model;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Топоним
 */

@Entity(name = "Toponym")
@Table(name = "toponym")
@SequenceGenerator(name = "ToponymIdGen", sequenceName = "toponym_id_gen")
public class ToponymModel {

    private Integer id;

    private ToponymModel parent;

    private String name;

    private Map<MessageLocaleModel, ToponymNameModel> names = new HashMap<>();

    private Integer level;

    private CountryModel country;

    private String code;

    private Integer type;

    @Id
    @GeneratedValue(generator = "ToponymIdGen")
    @Column(name = "toponym_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ownertoponym_id")
    public ToponymModel getParent() {
        return parent;
    }

    public void setParent(ToponymModel parent) {
        this.parent = parent;
    }

    @Column(name = "toponym_name", nullable = false, length = 200)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy="toponym")
    @MapKey(name="locale")
    public Map<MessageLocaleModel, ToponymNameModel> getNames() {
        return names;
    }

    public void setNames(Map<MessageLocaleModel, ToponymNameModel> names) {
        this.names = names;
    }

    @Column(name = "toponym_level", nullable = false)
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    public CountryModel getCountry() {
        return country;
    }

    public void setCountry(CountryModel country) {
        this.country = country;
    }

    @Column(name = "toponym_code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "toponymtype_id")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ToponymModel{" +
                "type=" + type +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}

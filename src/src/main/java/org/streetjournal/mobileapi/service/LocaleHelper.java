package org.streetjournal.mobileapi.service;

import com.google.inject.ImplementedBy;

import java.util.Locale;

@ImplementedBy(LocaleHelperImpl.class)
public interface LocaleHelper {
    /**
     * Нормализует локаль. Если локали нет в списке поддерживаемых, то возвращает английскую локаль
     * @param locale входная локаль
     * @return нормализованная локаль
     */
    Locale normalize(Locale locale);
}

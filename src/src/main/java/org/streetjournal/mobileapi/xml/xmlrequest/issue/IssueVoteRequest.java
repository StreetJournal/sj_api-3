package org.streetjournal.mobileapi.xml.xmlrequest.issue;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Запрос добавления голоса за сигнал
 */
@XmlRootElement(name = "Request")
public class IssueVoteRequest extends AbstractRequest {

    private String token;

    private Integer issueId;

    public IssueVoteRequest() {
    }

    @XmlAttribute(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlElement(name = "IssueId")
    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

}

package org.streetjournal.mobileapi.api.issue;

import biz.gelicon.sjua.model.IssueCommentModel;
import biz.gelicon.sjua.model.IssueCommentTypeModel;
import biz.gelicon.sjua.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.api.images.ImageAccessHelper;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.object.ObjectDataRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.LocationData;
import org.streetjournal.mobileapi.xml.xmlresponse.object.ObjectData;
import org.streetjournal.mobileapi.xml.xmlresponse.object.ObjectDataResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 * Обработчик запроса данных по списку идентификаторов сигналов. Данные могут вернуться не в том
 * порядке, в каком перечислены идентификаторы.
 */
@javax.ws.rs.Path("get_object_data")
public class ObjectDataHandler {

    private static final Logger logger = LoggerFactory.getLogger(ObjectDataHandler.class);

    @Inject
    EntityManager em;

    @Inject
    ImageAccessHelper imageAccessHelper;

/*
    @Inject
    TicketControl ticketControl;

    private Integer userId;
*/
    private ObjectDataRequest request;

    private final List<ObjectData> result = new ArrayList<>();

    private final Map<Integer, ObjectData> resultMap = new HashMap<>();

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public AbstractResponse execute(ObjectDataRequest request) {

        this.request = request;

//        authentificate();

        prepareResult();

        prepareVotes();

        prepareEvents();

//        prepareMyVotes();

        return new ObjectDataResponse(result);

    }

    private void prepareVotes() {
        for (Tuple item :
            em.createQuery("SELECT e.issue.id,count(*) FROM IssueComment e WHERE e.issue.id IN :ids AND e.type.id=:type GROUP BY e.issue.id", Tuple.class)
                .setParameter("ids", request.getObjects())
                .setParameter("type", IssueCommentTypeModel.TYPE_VOTE)
                .getResultList()) {

            Integer id = item.get(0, Integer.class);
            Long count = item.get(1, Long.class);

            ObjectData data = resultMap.get(id);

            data.setVotes(count.intValue());
        }
    }

    private void prepareEvents() {

        for (Tuple item :
            em.createQuery("SELECT e.issue.id,count(*) FROM IssueComment e WHERE e.issue.id IN :ids GROUP BY e.issue.id", Tuple.class)
                .setParameter("ids", request.getObjects())
                .getResultList()) {

            Integer id = item.get(0, Integer.class);
            Long count = item.get(1, Long.class);

            ObjectData data = resultMap.get(id);

            data.setEvents(count.intValue());
        }

        for (Tuple item :
            em.createQuery(
                "SELECT e.issue.id,count(*) FROM IssueWatchArea e " +
                    "WHERE e.issue.id IN :ids AND e.notifyDate IS NOT NULL GROUP BY e.issue.id",
                Tuple.class
            )
                .setParameter("ids", request.getObjects())
                .getResultList()
            ) {

            Integer id = item.get(0, Integer.class);
            Long count = item.get(1, Long.class);

            ObjectData data = resultMap.get(id);

            data.setEvents(count.intValue() + data.getEvents());
        }

    }

/*
    private void prepareMyVotes() {

        if (userId == null) {
            return;
        }

        for (Tuple item :
            em.createQuery(
                "SELECT e.issue.id " +
                    "FROM IssueComment e " +
                    "WHERE e.issue.id IN :ids AND e.type.id=:type AND e.user.id=:userId", Tuple.class)
                .setParameter("ids", request.getObjects())
                .setParameter("type", IssueCommentTypeModel.TYPE_VOTE)
                .setParameter("userId", userId)
                .getResultList()) {

            Integer id = item.get(0, Integer.class);

            ObjectData data = resultMap.get(id);

            data.setVote(true);
        }

    }
*/
    private void prepareResult() {

        for (IssueCommentModel item :
            em.createQuery("SELECT e FROM IssueComment e JOIN FETCH e.issue " +
                "WHERE e.issue.id IN :ids AND e.replyTo IS NULL", IssueCommentModel.class)
                .setParameter("ids", request.getObjects()).getResultList()) {

            ObjectData data = create(item);

            resultMap.put(item.getIssue().getId(), data);
        }

        for(Integer item : request.getObjects()) {
            result.add(resultMap.get(item));
        }

    }

    private ObjectData create(IssueCommentModel item) {

        ObjectData data = new ObjectData();

        data.setId(item.getIssue().getId());

        data.setSerial(createSerial(item));

        data.setName(item.getIssue().getName());
        data.setCreationDate(item.getCreationDate());
        data.setUpdateDate(item.getModificationDate());

        data.setAddress(item.getIssue().getAddress());

/*
        switch (item.getIssue().getMailMode()) {
            case IssueModel.CANEMAIL_SPAM:
                data.setModerationStatus(ModerationStatus.DISCARTED);
                break;
            case IssueModel.CANEMAIL_PUBLISHED:
                break;
            case IssueModel.CANEMAIL_PRIVATE:
                break;
            default:
                data.setModerationStatus(ModerationStatus.REVIEW);
        }
*/

//        String t = item.getText();

//        if (t != null && t.indexOf(0) >= 0) {
//            System.out.println(t);
//        }

        data.setDescription(item.getText());

//        logger.info("Status for {} is {}", item.getIssue().getId(), item.getIssue().getStatus().getId());
//        data.setStatus(StatusTypeConvertor.decode(item.getIssue().getStatus().getId()));

        Double lat = item.getIssue().getLalitude();
        Double lng = item.getIssue().getLongitude();

        if (lat != null && lng != null) {
            data.setLocation(new LocationData((int) Math.round(lat * 1E6), (int) Math.round(lng * 1E6)));
        }

/*
        if (!item.getAnonymous() || item.getUser().getId().equals(userId)) {
            data.setAuthor(new AuthorData(item.getUser().getId(), formatName(item.getUser())));
        } else {
            data.setAuthor(new AuthorData(null, ""));
        }
*/
        data.setImages(imageAccessHelper.getPreviewImages(item));

//        data.setClarifications(getClarifications(item.getIssue()));

        return data;

    }

    private Long createSerial(IssueCommentModel item) {

        Date date1 = item.getIssue().getModerationDate();
        Date date2 = item.getModificationDate();

        if (date1 != null && date1.after(date2)) {
            return date1.getTime();
        } else {
            return date2.getTime();
        }

    }

    private static String formatName(UserModel user) {

        StringBuilder sb = new StringBuilder();

        appendWord(sb, user.getName());
//        appendWord(sb, user.getPatronymic());
        appendWord(sb, user.getSurname());

        return sb.toString();

    }

    private static void appendWord(StringBuilder stringBuilder, String value) {

        if (value != null && value.length() > 0) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(' ');
            }
            stringBuilder.append(value);
        }

    }

/*
    private List<ClarificationData> getClarifications(IssueModel issue) {

        List<ClarificationData> result = new ArrayList<>();

        Map<IssueCommentModel, ClarificationData> map = new HashMap<>();

        for (IssueCommentModel item : em.createQuery(
            "SELECT e " +
                "FROM IssueComment e " +
                "WHERE e.issue=:issue AND e.type.id=:type", IssueCommentModel.class)
            .setParameter("issue", issue)
            .setParameter("type", IssueCommentTypeModel.TYPE_ADDITION)
            .getResultList()) {

            ClarificationData data = new ClarificationData();

            data.setDate(item.getCreationDate());
            data.setText(item.getText());

            result.add(data);

            map.put(item, data);

        }

        for (Map.Entry<IssueCommentModel, List<ImageData>> entry :
            imageAccessHelper.getPreviewImages(map.keySet()).entrySet()) {

            ClarificationData data = map.get(entry.getKey());

            if (data == null) {
                continue;
            }

            data.setImages(entry.getValue());
        }

        return result;
    }
*/

/*
    private void authentificate() {
        if (request.getToken() != null) {
            ticketControl.setHexTicket(request.getToken());
            userId = ticketControl.getIntValue();
        }
    }
*/

}

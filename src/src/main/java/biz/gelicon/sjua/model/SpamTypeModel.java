package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Тип жалобы на спам
 */
@Entity(name = "SpamTypeType")
@Table(name = "spamtype")
@SequenceGenerator(name = "SpamTypeIdGen", sequenceName = "spamtype_id_gen")
public class SpamTypeModel {

  private Integer id;

  private String name;

  @Id
  @GeneratedValue(generator = "SpamTypeIdGen")
  @Column(name = "spamtype_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Название типа организации (нелокализованное)
   * 
   * @return
   */
  @Column(name = "spamtype_name", length = 255, nullable = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}

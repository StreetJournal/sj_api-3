package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Тип комментария
 */
@Entity(name = "IssueCommentType")
@Table(name = "issuecommenttype")
//@SequenceGenerator(name = "IssueCommentTypeIdGen", sequenceName = "issuecommenttype_id_gen")
public class IssueCommentTypeModel {

  public static final int TYPE_COMMENT = 1;

  public static final int TYPE_VOTE = 2;

  public static final int TYPE_STATUS = 3;

  public static final int TYPE_ADDITION = 4;

  private Integer id;

  private String name;

  @Id
//  @GeneratedValue(generator = "IssueCommentTypeIdGen")
  @Column(name = "issuecommenttype_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "issuecommenttype_name", nullable = false, length = 20)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}

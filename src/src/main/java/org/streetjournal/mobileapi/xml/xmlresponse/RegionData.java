package org.streetjournal.mobileapi.xml.xmlresponse;

import org.streetjournal.mobileapi.xml.Bound;
import org.streetjournal.mobileapi.xml.BoundAdapter;
import org.streetjournal.mobileapi.xml.Localized;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Region")
public class RegionData {

    private String code;

    private String name;

    private List<Localized> localizedNames = new ArrayList<>();

    private String parent;

    private List<RegionData> childs;

    private Bound bound;

    public RegionData() {
    }

    @XmlAttribute(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlElement(name = "Parent")
    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    @XmlElement(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "LocalizedName")
    public List<Localized> getLocalizedNames() {
        return localizedNames;
    }

    public void setLocalizedNames(List<Localized> localizedNames) {
        this.localizedNames = localizedNames;
    }

    @XmlElementRef
    public List<RegionData> getChilds() {
        return childs;
    }

    public void setChilds(List<RegionData> childs) {
        this.childs = childs;
    }

    @XmlJavaTypeAdapter(BoundAdapter.class)
    @XmlElement(name = "SWNE")
    public Bound getBound() {
        return bound;
    }

    public void setBound(Bound bound) {
        this.bound = bound;
    }

}

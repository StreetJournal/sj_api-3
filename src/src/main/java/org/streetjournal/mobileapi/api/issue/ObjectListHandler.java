package org.streetjournal.mobileapi.api.issue;

import biz.gelicon.sjua.model.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.config.Configuration;
import org.streetjournal.mobileapi.service.ToponymTreeHelper;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.SortOrder;
import org.streetjournal.mobileapi.xml.xmlrequest.object.ObjectListRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.object.ObjectData;
import org.streetjournal.mobileapi.xml.xmlresponse.object.ObjectDataResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 *
 */
@javax.ws.rs.Path("get_object_list")
public class ObjectListHandler {

    private static final Logger logger = LoggerFactory.getLogger(ObjectListHandler.class);

    @Inject
    EntityManager em;

    @Inject
    CriteriaBuilder cb;

    @Inject
    ToponymTreeHelper toponymTreeHelper;

    @Inject
    private Configuration configuration;

    private List<LayerModel> layers;

    private List<ObjectData> result;

    private ObjectListRequest request;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces({MediaType.APPLICATION_XML})
    public AbstractResponse execute(ObjectListRequest request) {

        this.request = request;

        resolveLayers();

        if (layers.size() > 0) {

            prepareResult();

            if (request.getOrder() == SortOrder.VOTES) {
                resortByVotes();
            }

        }

        return new ObjectDataResponse(result);

    }

    private void resortByVotes() {

        // @TODO Нужно переделать, т.к. при большом числе записей сортировка может быть невенрой

        if (result.size() == 0) {
            return;
        }

        Map<Integer, ObjectData> objectsMap = new HashMap<>();
        List<Integer> objects = new ArrayList<>();

        for (ObjectData item : result) {
            objects.add(item.getId());
            objectsMap.put(item.getId(), item);
        }

        CriteriaQuery<Integer> q = cb.createQuery(Integer.class);

        Root<IssueCommentModel> root = q.from(IssueCommentModel.class);

        q.select(root.get("issue").<Integer>get("id"));

        List<Expression<Boolean>> filter = new ArrayList<>();

        filter.add(cb.equal(root.get("type").get("id"), IssueCommentTypeModel.TYPE_VOTE));
        filter.add(root.get("issue").get("id").in(objects));

        Predicate ands = cb.conjunction();
        ands.getExpressions().addAll(filter);
        q.where(ands);

        q.groupBy(root.get("issue"));

        q.orderBy(cb.desc(cb.count(root.get("issue"))), cb.desc(cb.greatest(root.<Date>get("creationDate"))));

        TypedQuery<Integer> query = em.createQuery(q);

        List<Integer> sortOrder = query.getResultList();

        logger.info("Fetched: " + sortOrder);

        List<ObjectData> resortedData = new ArrayList<>();

        for (Integer item : sortOrder) {

            ObjectData data = objectsMap.get(item);

            if (data == null) {
                continue;
            }

            resortedData.add(data);

            result.remove(data);

        }

//    result.removeAll(issuesMap.values());

        resortedData.addAll(result);

        result = resortedData;

    }

    private void resolveLayers() {

        if (request.getCategories() != null) {
            layers = em.createQuery("SELECT e FROM Layer e WHERE e.code IN (:code)" +
                    " AND e.status=:status", LayerModel.class)
                    .setParameter("code", request.getCategories())
                    .setParameter("status", LayerModel.STATUS_VISIBLE)
                    .getResultList();
        } else {
            layers = em.createQuery("SELECT e FROM Layer e WHERE e.code LIKE '3%'" +
                    " AND e.status=:status", LayerModel.class)
//                    .setParameter("code", request.getCategories())
                    .setParameter("status", LayerModel.STATUS_VISIBLE)
                    .getResultList();
        }

    }

    private void prepareResult() {

        boolean useSerial = request.getUseSerial() != null && request.getUseSerial();

        CriteriaQuery<Tuple> q = cb.createTupleQuery();

        Root<IssueCommentModel> root = q.from(IssueCommentModel.class);

        if (useSerial) {
            q.select(cb.tuple(root.get("issue").get("id"), root.get("modificationDate"), root.get("issue").get("moderationDate")));
        } else {
            q.select(cb.tuple(root.get("issue").get("id")));
        }

        Path<IssueModel> issue = root.get("issue");

        applyFilter(q, cb.isNull(root.get("replyTo")), issue);
        applySort(q, root);

        TypedQuery<Tuple> query = em.createQuery(q);

        query.setFirstResult(0);

        int limit = request.getLimit() != null ? Math.min(request.getLimit(), 1000) : 1000;

        query.setMaxResults(limit);

        result = new ArrayList<>();

        for (Tuple item : query.getResultList()) {

            ObjectData data = new ObjectData();

            data.setId(item.get(0, Integer.class));

            if (useSerial) {

                Date date1 = item.get(1, Date.class);
                Date date2 = item.get(2, Date.class);

                long serial;

                if (date2 != null && date1.after(date2)) {
                    serial = date1.getTime();
                } else {
                    serial = date2.getTime();
                }

                data.setSerial(serial);

            }

            result.add(data);

        }

    }

    private void applyFilter(CriteriaQuery<?> query, Predicate filter, Path<IssueModel> root) {

        List<Expression<Boolean>> result = new ArrayList<>();

        if (filter != null) {
            result.add(filter);
        }

        // Проверка на модерацию
        result.add(cb.equal(root.get("mailMode"), IssueModel.CANEMAIL_PUBLISHED));


        if (request.getRegion() != null || StringUtils.isNotBlank(configuration.regionCode)) {
            String country;
            String code;
            if (request.getRegion() != null) {
                country = request.getRegion().substring(0, 2);
                code = request.getRegion().substring(2);
            } else {
                country = configuration.regionCode.substring(0, 2);
                code = configuration.regionCode.substring(2);
            }

            ToponymModel toponymModel = em.createQuery(
                    "SELECT e FROM Toponym e WHERE e.country.code=:country AND e.code=:code",
                    ToponymModel.class)
                    .setParameter("country", country)
                    .setParameter("code", code)
                    .getSingleResult();

            List<ToponymModel> regions = toponymTreeHelper.getToponymTreeSection(toponymModel);

            result.add(root.get("toponym").in(regions));

        }

        if (request.getLayerType() != null) {
            int type = 1;
            switch (request.getLayerType()) {
                case 2:
                    type = IssueStatusModel.STATUS_PUBLIC_TYPE_2;
                    break;
                case 3:
                    type = IssueStatusModel.STATUS_PUBLIC_TYPE_3;
                    break;
                case 4:
                    type = IssueStatusModel.STATUS_PUBLIC_TYPE_4;
                    break;
            }
            result.add(root.get("status").get("id").in(type));
        } else if (layers != null && layers.size() > 0) {
            Subquery<IssueModel> subquery = query.subquery(IssueModel.class);
            Root<IssueLayerBindModel> subqueryRoot = subquery.from(IssueLayerBindModel.class);
            subquery.select(subqueryRoot.<IssueModel>get("issue"));
            subquery.where(subqueryRoot.get("layer").in(layers));
            result.add(root.in(subquery));
            result.add(root.get("status").get("id").in(
                            IssueStatusModel.STATUS_OPEN, IssueStatusModel.STATUS_PUBLIC_TYPE_2, IssueStatusModel.STATUS_PUBLIC_TYPE_3, IssueStatusModel.STATUS_PUBLIC_TYPE_4)
            );
//            .from(Issue)
        }

//    if(layers != null) {
//
//      List<Expression<Boolean>> list = new ArrayList<>();
//
//      Expression<Collection<LayerModel>> ls = root.get("layers");
//
//      for(LayerModel item : layers) {
//        list.add(cb.isMember(item, ls));
//      }
//
//      Predicate ors = cb.disjunction();
//
//      ors.getExpressions().addAll(list);
//
//      result.add(ors);
//
//    }

        if (result.size() > 0) {
            Predicate ands = cb.conjunction();
            ands.getExpressions().addAll(result);
            query.where(ands);
        }

    }

    private void applySort(CriteriaQuery<?> query, Path<IssueCommentModel> root) {

        Map<String, Path<?>> result = new HashMap<>();

//    result.put("id", root.get("issue").get("id"));
//    result.put("name", root.get("issue").get("name"));
//    result.put("creationDate", root.get("creationDate"));
//    result.put("modificationDate", root.get("modificationDate"));

        applySort(query, result, cb.desc(root.get("creationDate")));

    }

    private void applySort(CriteriaQuery<?> query, Map<String, Path<?>> map, Order defaultOrder) {

        List<Order> orders = new ArrayList<>();

        if (orders.size() > 0) {
            query.orderBy(orders);
        } else {
            query.orderBy(defaultOrder);
        }

    }

}

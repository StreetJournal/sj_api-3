package biz.gelicon.server.service;

import biz.gelicon.server.api.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
@Managed
public class UserIdValueHolder {

  private static final Logger logger = LoggerFactory.getLogger(UserIdValueHolder.class);

  private Integer userId;

  public UserIdValueHolder() {
    logger.info("Create new holder");
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public void close() {
    logger.info("Dispose holder content");
  }

}

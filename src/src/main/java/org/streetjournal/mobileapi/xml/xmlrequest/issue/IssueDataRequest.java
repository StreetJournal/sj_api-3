package org.streetjournal.mobileapi.xml.xmlrequest.issue;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Зарос основных данных о проблеме по списку идентификаторов проблем.
 */
@XmlRootElement(name = "Request")
public class IssueDataRequest extends AbstractRequest {

    private String token;

    private List<Integer> issues;

    public IssueDataRequest() {
    }

    public IssueDataRequest(List<Integer> issues) {
        this.issues = issues;
    }

    @XmlAttribute(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlElement(name = "IssueId")
    public List<Integer> getIssues() {
        return issues;
    }

    public void setIssues(List<Integer> issues) {
        this.issues = issues;
    }

}

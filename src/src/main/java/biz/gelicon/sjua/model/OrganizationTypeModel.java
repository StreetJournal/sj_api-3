package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Тип организации
 */
@Entity(name = "OrganizationType")
@Table(name = "organizationtype")
@SequenceGenerator(name = "OrganizationTypeIdGen", sequenceName = "organizationtype_id_gen")
public class OrganizationTypeModel {

  private Integer id;

  private String name;

  private byte[] imageData;

  @Id
  @GeneratedValue(generator = "OrganizationTypeIdGen")
  @Column(name = "organizationtype_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Название типа организации (нелокализованное)
   *
   * @return
   */
  @Column(name = "organizationtype_name", length = 255, nullable = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  /**
   * Иконка типа организации в формате PNG
   *
   * @return
   */
  @Lob
  @Column(name = "organizationtype_image", nullable = true)
  public byte[] getImageData() {
    return imageData;
  }

  public void setImageData(byte[] imageData) {
    this.imageData = imageData;
  }

}

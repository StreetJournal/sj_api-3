package org.streetjournal.mobileapi.xml;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Ответ, содержащий только код ответа без дополнительных данных
 */
@XmlRootElement(name = "Response")
public class NoDataResponse extends AbstractResponse {

    public NoDataResponse() {
        this(0);
    }

    public NoDataResponse(int statusCode) {
        super(statusCode, null);
    }

    public NoDataResponse(int statusCode, String note) {
        super(statusCode, note);
    }

    public NoDataResponse(int statusCode, String code, String note) {
        super(statusCode, code, note);
    }

}

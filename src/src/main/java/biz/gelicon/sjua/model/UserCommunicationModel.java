package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 */
@Entity(name = "UserCommunication")
@Table(name = "progusercommunication")
@SequenceGenerator(name = "UserCommunicationIdGen", sequenceName = "progusercommunication_id_gen")
public class UserCommunicationModel {

  private Integer id;

  private UserModel user;

  private String value;

  private CommunicationTypeModel type;

  private Integer status;

  @Id
  @GeneratedValue(generator = "UserCommunicationIdGen")
  @Column(name = "progusercommunication_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @ManyToOne
  @JoinColumn(name = "proguser_id", nullable = false)
  public UserModel getUser() {
    return user;
  }

  public void setUser(UserModel user) {
    this.user = user;
  }

  @Column(name = "progusercommunication_value", nullable = false, length = 255)
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @ManyToOne
  @JoinColumn(name = "communicationtype_id", nullable = false)
  public CommunicationTypeModel getType() {
    return type;
  }

  public void setType(CommunicationTypeModel type) {
    this.type = type;
  }

  @Column(name = "progusercommunication_status", nullable = false)
  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

}

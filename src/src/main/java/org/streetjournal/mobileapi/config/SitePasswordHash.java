package org.streetjournal.mobileapi.config;

import biz.gelicon.server.service.PasswordHash;

/**
 *
 */
public class SitePasswordHash extends PasswordHash {

  public SitePasswordHash() {
    super("UTF-8", "MD5", 0, 32);
  }

}

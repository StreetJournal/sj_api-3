package biz.gelicon.server.modules;

import biz.gelicon.server.api.Managed;
import biz.gelicon.server.config.DatabaseConfig;
import biz.gelicon.server.service.EntityManagerHolder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class DatabaseModule extends AbstractModule {

  private static final Logger logger = LoggerFactory.getLogger(DatabaseModule.class);

  private final String persistence;

  public DatabaseModule(String persistence) {
    this.persistence = persistence;
  }

  @Override
  protected void configure() {
  }

  @Provides
  @Singleton
  public EntityManagerFactory getEntityManagerFactiry(DatabaseConfig databaseConfig) {

    logger.info("Setup JPA datasource");

    Map<String, String> params = new HashMap<String, String>();

    params.put("javax.persistence.jdbc.user", databaseConfig.user);
    params.put("javax.persistence.jdbc.password", databaseConfig.password);
    params.put("javax.persistence.jdbc.url", databaseConfig.url);

    params.put("hibernate.connection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider");
    params.put("hibernate.c3p0.testConnectionOnCheckout", "true");

//      <property name="hibernate.connection.provider_class"
//      value="org.hibernate.connection.C3P0ConnectionProvider" />
//
//      <property name="hibernate.c3p0.max_size" value="100" />
//      <property name="hibernate.c3p0.min_size" value="0" />
//      <property name="hibernate.c3p0.acquire_increment" value="1" />
//      <property name="hibernate.c3p0.idle_test_period" value="300" />
//      <property name="hibernate.c3p0.max_statements" value="0" />
//      <property name="hibernate.c3p0.timeout" value="100" />

    EntityManagerFactory result = Persistence.createEntityManagerFactory(persistence, params);

    logger.info("Configure database url={} user={}", databaseConfig.url, databaseConfig.user);

    return result;

  }

  @Provides
  @Managed
  public EntityManagerHolder getEntityManagerHolder() {
    return new EntityManagerHolder();
  }

  @Provides
  public EntityManager getEntityManager(EntityManagerFactory entityManagerFactory,
                                        EntityManagerHolder entityManagerHolder) {

    if(entityManagerHolder.getEntityManager() == null) {

      logger.info("Allocate new EntityManager");

      entityManagerHolder.setEntityManager(entityManagerFactory.createEntityManager());

    }

    return entityManagerHolder.getEntityManager();

  }

  @Provides
  public CriteriaBuilder getCriteriaBuider(EntityManager entityManager) {
    return entityManager.getCriteriaBuilder();
  }

}

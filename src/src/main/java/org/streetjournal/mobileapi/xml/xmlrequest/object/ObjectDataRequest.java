package org.streetjournal.mobileapi.xml.xmlrequest.object;

import org.streetjournal.mobileapi.xml.AbstractRequest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Зарос основных данных о проблеме по списку идентификаторов проблем.
 */
@XmlRootElement(name = "Request")
public class ObjectDataRequest extends AbstractRequest {

    private String token;

    private List<Integer> objects;

    public ObjectDataRequest() {
    }

    public ObjectDataRequest(List<Integer> objects) {
        this.objects = objects;
    }

    @XmlAttribute(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlElement(name = "ObjectId")
    public List<Integer> getObjects() {
        return objects;
    }

    public void setObjects(List<Integer> objects) {
        this.objects = objects;
    }

}

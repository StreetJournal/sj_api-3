package org.streetjournal.mobileapi.api.issue;

import biz.gelicon.sjua.model.IssueStatusModel;
import org.streetjournal.mobileapi.xml.IssueStatus;

/**
 *
 */
public class StatusTypeConvertor {

    public static Integer encode(IssueStatus status) {
        switch (status) {
            case ACCEPTED:
                return IssueStatusModel.STATUS_ACCEPTED;
            case OPEN:
                return IssueStatusModel.STATUS_OPEN;
            case CLOSED:
                return IssueStatusModel.STATUS_CLOSED;
            case ARCHIVED:
                return IssueStatusModel.STATUS_ARCHIVE;
        }
        return null;
    }

    public static IssueStatus decode(Integer status) {
        switch (status) {
            case IssueStatusModel.STATUS_ACCEPTED:
                return IssueStatus.ACCEPTED;
            case IssueStatusModel.STATUS_OPEN:
                return IssueStatus.OPEN;
            case IssueStatusModel.STATUS_CLOSED:
                return IssueStatus.CLOSED;
            case IssueStatusModel.STATUS_ARCHIVE:
                return IssueStatus.ARCHIVED;
        }
        return null;
    }

}

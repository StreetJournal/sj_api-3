package org.streetjournal.mobileapi.api.regions;

import biz.gelicon.sjua.model.ToponymModel;
import biz.gelicon.sjua.model.ToponymNameModel;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.regrect.RegionBoundsHelper;
import org.streetjournal.mobileapi.service.LocaleHelper;
import org.streetjournal.mobileapi.service.ToponymTreeHelper;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.Localized;
import org.streetjournal.mobileapi.xml.xmlrequest.RegionListRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.RegionData;
import org.streetjournal.mobileapi.xml.xmlresponse.RegionListResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@javax.ws.rs.Path("get_region_tree")
public class RegionTreeHandler {

    private static final Logger logger = LoggerFactory.getLogger(RegionTreeHandler.class);

    @Inject
    EntityManager em;
    @Inject
    ToponymTreeHelper toponymTreeHelper;
    @Inject
    RegionBoundsHelper regionBoundsHelper;
    @Inject
    LocaleHelper localeHelper;

    private final List<RegionData> result = new ArrayList<>();
    private final Map<ToponymModel, List<ToponymNameModel>> map = new HashMap<>();

    private RegionListRequest request;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public AbstractResponse execute(RegionListRequest request) {

        this.request = request.setLocale(localeHelper.normalize(request.getLocale()));
        //TODO: правильнее будет во время выборки топонимов джойнить только нужную локаль. Необходим большой рефакторинг
        cacheNames();

        prepareResult();

        return new RegionListResponse(result);

    }

    private void cacheNames() {

        for (ToponymNameModel item : em.createQuery("SELECT e FROM ToponymName e",
                ToponymNameModel.class).getResultList()) {

            List<ToponymNameModel> list = map.get(item.getToponym());

            if (list == null) {
                map.put(item.getToponym(), list = new ArrayList<>());
            }

            list.add(item);

        }
    }

    private void prepareResult() {

        for (ToponymModel roots : toponymTreeHelper.getRoots()) {
            result.add(createHive(roots));
        }

    }

    private RegionData createHive(ToponymModel root) {

        RegionData result = new RegionData();

        String code = root.getCountry().getCode() + root.getCode();
        result.setCode(code);
        result.setName(root.getName());
        result.setBound(regionBoundsHelper.getRegionBound(code));

        List<ToponymNameModel> names = map.get(root);

        if (names != null) {
            if (null != request.getLocale()) {
                result.setLocalizedNames(null);
                ToponymNameModel nameModel = Iterables.find(names, new Predicate<ToponymNameModel>() {
                    @Override
                    public boolean apply(ToponymNameModel toponymNameModel) {
                        return request.getLocale().getLanguage().equals(toponymNameModel.getLocale().getCode());
                    }
                }, null);
                if (null != nameModel) {
                    result.setName(nameModel.getName());
                }
            } else {
                for (ToponymNameModel item : names) {
                    result.getLocalizedNames().add(new Localized(item.getLocale().getCode(), item.getName()));
                }
            }
        }

        List<RegionData> children = new ArrayList<>();

        for (ToponymModel item : toponymTreeHelper.getChilds(root)) {
            children.add(createHive(item));
        }

        result.setChilds(children);

        return result;

    }

}

package biz.gelicon.sjua.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 */
@Entity(name = "Spam")
@Table(name = "spam")
@SequenceGenerator(name = "SpamIdGen", sequenceName = "spam_id_gen")
public class SpamModel {

  private Integer id;

  private IssueCommentModel comment;

  private SpamTypeModel type;

  private UserModel user;

  private Date date;

  private String text;

  @Id
  @GeneratedValue(generator = "SpamIdGen")
  @Column(name = "spam_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @ManyToOne
  @JoinColumn(name = "issuecomment_id", nullable = false)
  public IssueCommentModel getComment() {
    return comment;
  }

  public void setComment(IssueCommentModel comment) {
    this.comment = comment;
  }

  @ManyToOne
  @JoinColumn(name = "spamtype_id", nullable = false)
  public SpamTypeModel getType() {
    return type;
  }

  public void setType(SpamTypeModel type) {
    this.type = type;
  }

  @ManyToOne
  @JoinColumn(name = "proguser_id", nullable = false)
  public UserModel getUser() {
    return user;
  }

  public void setUser(UserModel user) {
    this.user = user;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "spam_date", nullable = false)
  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Lob
  @Column(name = "spam_text")
  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

}

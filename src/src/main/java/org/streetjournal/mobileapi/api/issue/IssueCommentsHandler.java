package org.streetjournal.mobileapi.api.issue;

import biz.gelicon.sjua.model.IssueCommentModel;
import biz.gelicon.sjua.model.IssueCommentTypeModel;
import biz.gelicon.sjua.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.api.images.ImageAccessHelper;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.issue.IssueCommentsRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.AuthorData;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.ImageData;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.IssueCommentData;
import org.streetjournal.mobileapi.xml.xmlresponse.issue.IssueCommentsResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@javax.ws.rs.Path("get_issue_comments")
public class IssueCommentsHandler {

    private static final Logger logger = LoggerFactory.getLogger(IssueCommentsHandler.class);

    @Inject
    EntityManager em;

    @Inject
    ImageAccessHelper imageAccessHelper;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces({MediaType.APPLICATION_XML})
//    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public AbstractResponse execute(IssueCommentsRequest request) {

        List<IssueCommentData> result = new ArrayList<>();

        Map<IssueCommentModel, IssueCommentData> map = new HashMap<>();

        IssueCommentModel rootComment = em.createQuery(
            "SELECT e FROM IssueComment e " +
                "WHERE e.issue.id=:issueId AND e.replyTo IS NULL", IssueCommentModel.class
        )
            .setParameter("issueId", request.getIssueId())
            .getSingleResult();

        List<Integer> types = new ArrayList<>();
        types.add(IssueCommentTypeModel.TYPE_COMMENT);
        types.add(IssueCommentTypeModel.TYPE_VOTE);
        types.add(IssueCommentTypeModel.TYPE_STATUS);

        for (IssueCommentModel item : em.createQuery(
            "SELECT e FROM IssueComment e " +
                "WHERE e.issue.id=:issueId AND e.deleteStatus<>:status " +
//                "WHERE e.issue.id=:issueId AND e.replyTo IS NOT NULL AND e.deleteStatus<>:status " +
                "AND e.type.id IN(:types)" +
                "ORDER BY e.creationDate", IssueCommentModel.class
        )
            .setParameter("issueId", request.getIssueId())
            .setParameter("status", IssueCommentModel.STATUS_DELETED)
            .setParameter("types", types)
            .getResultList()) {

            IssueCommentData data = new IssueCommentData();

            data.setId(item.getId());

            if (item.getReplyTo() != null && item.getReplyTo() != rootComment) {
                data.setReplyToId(item.getReplyTo().getId());
            }

            data.setCreated(item.getCreationDate());

            if (!item.getAnonymous()) {
                data.setAuthor(new AuthorData(item.getUser().getId(), formatName(item.getUser())));
            } else {
                data.setAuthor(new AuthorData(null, ""));
            }

            if (item.getReplyTo() != null) {
                data.setText(item.getText());
                map.put(item, data);
            }

            switch (item.getType().getId()) {
                case IssueCommentTypeModel.TYPE_VOTE:
                    data.setVote(true);
                    break;
                case IssueCommentTypeModel.TYPE_STATUS:
                    data.setStatus(StatusTypeConvertor.decode(item.getIssueStatus().getId()));
                    break;
            }

            result.add(data);
        }

        if (map.size() > 0) {
            for (Map.Entry<IssueCommentModel, List<ImageData>> entry :
                imageAccessHelper.getPreviewImages(map.keySet()).entrySet()) {

                IssueCommentData data = map.get(entry.getKey());

                if (data == null) {
                    continue;
                }

                data.setImages(entry.getValue());
            }
        }

        return new IssueCommentsResponse(result);
    }

    private static String formatName(UserModel user) {

        StringBuilder sb = new StringBuilder();

        appendWord(sb, user.getName());
//        appendWord(sb, user.getPatronymic());
        appendWord(sb, user.getSurname());

        return sb.toString();

    }

    private static void appendWord(StringBuilder stringBuilder, String value) {

        if (value != null && value.length() > 0) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(' ');
            }
            stringBuilder.append(value);
        }

    }

}

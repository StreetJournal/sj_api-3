package org.streetjournal.mobileapi.service;

import com.google.common.collect.Sets;

import java.util.Locale;
import java.util.Set;

public class LocaleHelperImpl implements LocaleHelper {

    private static Set<String> SUPPORTED_LANGUAGES = Sets.newHashSet("ru","uk","en");

    @Override
    public Locale normalize(Locale locale) {
        if (null == locale || SUPPORTED_LANGUAGES.contains(locale.getLanguage())) {
            return locale;
        }
        return Locale.ENGLISH;
    }
}

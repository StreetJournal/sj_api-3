package org.streetjournal.mobileapi.api.issue;

import org.streetjournal.mobileapi.api.images.ImageAccessHelper;

import javax.inject.Inject;
import javax.ws.rs.*;

/**
 *
 */
@Path("get_issue_image")
public class GetIssueImageHandler {

    @Inject
    ImageAccessHelper imageAccessHelper;

    @GET
    @Path("{image}")
    @Produces("image/jpeg")
    public byte[] get(@PathParam("image") Integer imageId,
                      @DefaultValue("false") @QueryParam("preview") boolean preview) {
        return imageAccessHelper.getImageData(imageId, preview);
    }

}

package org.streetjournal.mobileapi.regrect;

import org.streetjournal.mobileapi.xml.Bound;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class RegionBoundsHelper {

    private final Map<String, Bound> map;

    public RegionBoundsHelper(Map<String, Bound> map) {
        this.map = new HashMap<>(map);
    }

    public Bound getRegionBound(String code) {
        return map.get(code);
    }

}

package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Модель связи области контроля/организации и проблему
 */

@Entity(name = "IssueWatchArea")
@Table(name = "issuewatcharea")
@SequenceGenerator(name = "IssueWatchAreaIdGen", sequenceName = "issuewatcharea_id_gen")
public class IssueWatchAreaModel {

  private Integer id;

  private WatchAreaModel organization;

  private IssueModel issue;

  private Date notifyDate;

  private CommunicationTypeModel communication;

  private UserModel userInitiator;

  private WatchAreaModel organizationInitiator;

  private Boolean notifyEnabled;

  @Id
  @GeneratedValue(generator = "IssueWatchAreaIdGen")
  @Column(name = "issuewatcharea_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @ManyToOne
  @JoinColumn(name = "watcharea_id", nullable = false)
  public WatchAreaModel getOrganization() {
    return organization;
  }

  public void setOrganization(WatchAreaModel organization) {
    this.organization = organization;
  }

  @ManyToOne
  @JoinColumn(name = "issue_id", nullable = false)
  public IssueModel getIssue() {
    return issue;
  }

  public void setIssue(IssueModel issue) {
    this.issue = issue;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "issuewatcharea_datenotify", nullable = true)
  public Date getNotifyDate() {
    return notifyDate;
  }

  public void setNotifyDate(Date notifyDate) {
    this.notifyDate = notifyDate;
  }

  @ManyToOne
  @JoinColumn(name = "communicationtype_id", nullable = false)
  public CommunicationTypeModel getCommunication() {
    return communication;
  }

  public void setCommunication(CommunicationTypeModel communication) {
    this.communication = communication;
  }

  @ManyToOne
  @JoinColumn(name = "proguser_id", nullable = true)
  public UserModel getUserInitiator() {
    return userInitiator;
  }

  public void setUserInitiator(UserModel userInitiator) {
    this.userInitiator = userInitiator;
  }

  @ManyToOne
  @JoinColumn(name = "sourcewatcharea_id", nullable = true)
  public WatchAreaModel getOrganizationInitiator() {
    return organizationInitiator;
  }

  public void setOrganizationInitiator(WatchAreaModel organizationInitiator) {
    this.organizationInitiator = organizationInitiator;
  }

  // @TODO почему "nullable = true" в базе?
  @Column(name = "issuewatcharea_cannotify", columnDefinition = "integer", nullable = true)
  public Boolean getNotifyEnabled() {
    return notifyEnabled;
  }

  public void setNotifyEnabled(Boolean notifyEnabled) {
    this.notifyEnabled = notifyEnabled;
  }

}

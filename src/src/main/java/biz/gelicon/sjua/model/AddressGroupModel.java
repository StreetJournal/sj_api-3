package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Группа адресов
 *
 * @TODO проверить актуальность
 */
@Entity(name = "AddressGroup")
@Table(name = "addressgroup")
@SequenceGenerator(name="AddressGroupIdGen", sequenceName = "addressgroup_id_gen")
public class AddressGroupModel {

  private Integer id;

  @Id
  @GeneratedValue(generator="AddressGroupIdGen")
  @Column(name = "addressgroup_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

}

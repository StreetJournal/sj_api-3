package org.streetjournal.mobileapi.xml.xmlresponse.issue;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Ответ, содержащий идентификатор созданного комментария
 */
@XmlRootElement(name = "Response")
public class CreateObjectCommentResponse extends AbstractResponse {

    private Integer objectCommentId;

    public CreateObjectCommentResponse() {
        this(null);
    }

    public CreateObjectCommentResponse(Integer objectCommentId) {
        super(0, "Object comment created");
        this.objectCommentId = objectCommentId;
    }

    @XmlElement(name = "ObjectCommentId", required = true, nillable = false)
    public Integer getObjectCommentId() {
        return objectCommentId;
    }

    public void setObjectCommentId(Integer issueId) {
        this.objectCommentId = objectCommentId;
    }

}

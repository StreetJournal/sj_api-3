package biz.gelicon.sjua.model;

import javax.persistence.*;

@Entity(name = "AttributeRef")
@Table(name = "ATTRIBUTEREF")
@SequenceGenerator(name="AttributeRefIdGen", sequenceName = "attribute_ref_id_gen")
public class AttributeRef {

    private Integer id;

    @Id
    @GeneratedValue(generator="AttributeRefIdGen")
    @Column(name = "ATTRIBUTEREF_ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    
}

package org.streetjournal.mobileapi.xml.xmlresponse;

import org.streetjournal.mobileapi.xml.LocationAdapter;
import org.streetjournal.mobileapi.xml.TimestampAdapter;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 */
@XmlRootElement(name = "Location")
@XmlJavaTypeAdapter(LocationAdapter.class)
public class LocationData {

  private Integer lat;

  private Integer lng;

  public LocationData() {
  }

  public LocationData(Integer lat, Integer lng) {
    this.lat = lat;
    this.lng = lng;
  }

  public Integer getLat() {
    return lat;
  }

  public void setLat(Integer lat) {
    this.lat = lat;
  }

  public Integer getLng() {
    return lng;
  }

  public void setLng(Integer lng) {
    this.lng = lng;
  }

}

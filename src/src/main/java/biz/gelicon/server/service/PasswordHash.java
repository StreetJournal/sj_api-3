package biz.gelicon.server.service;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Класс для генерации хеша пароля и проверки пароля. Бинарные данные преобразуются в
 * шестнадцатиричные значения для представления в виде строки.
 * <p/>
 * Класс является легковесным и может создаваться непосредственно перед использованием.
 * <p/>
 * Отдельный экземпляр класса не является потокобезопасным.
 * <p/>
 * Экземпляры класса пользуются одним экземпляром генератора случайных чисел для получения соли, к
 * которому изпользуется потокобезопасных доступ.
 */
public class PasswordHash {

  private static final Random randomizer = new SecureRandom();

  private final String passwordEncoding;

  private final String hashAlg;

  private final MessageDigest messageDigest;

  private final int saltSize;

  private final int hashStringSize;

  /**
   * Современные реализации алгоритмов используют длину соли от 6 до 16 байт
   * <p/>
   * См. http://en.wikipedia.org/wiki/Salt_(cryptography)
   *
   * @param saltSize        Размер соли в байтах. При генерации, соль преобразуется в hex-значения, поэтому
   *                        на каждый байт соли добавляется 2 символа в хэш.
   * @param hashStringLimit Ограничение на общий размер хэша (включая соль) в символах.
   */
  public PasswordHash(String passwordEncoding, String hashAlg, int saltSize, int hashStringLimit) {

    this.passwordEncoding = passwordEncoding;
    this.hashAlg = hashAlg;

    this.saltSize = saltSize;

    try {
      messageDigest = MessageDigest.getInstance(hashAlg);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }

    hashStringSize = Math.min(hashStringLimit - saltSize * 2, messageDigest.getDigestLength() * 2);

  }

  /**
   * Хэширование пароля с солью
   *
   * @param password Пароль для хэширования
   * @return Страка-хэш пароля с солью, размером не больше параметра hashStringLimit, который был
   *         указан в конструкторе
   */
  public String hashPassword(String password) {

    byte salt[];

    if(saltSize>0) {

      salt = new byte[saltSize];

      synchronized (randomizer) {
        randomizer.nextBytes(salt);
      }

    } else {
      salt = null;
    }

    return hashPassword(salt, password);

  }

  /**
   * Проверка соответсвия пароля и хэша
   *
   * @param password Пароль
   * @param hash     Хэш с солью
   * @return true, если пароль соответствует хэшу, false если не соответствует
   */
  public boolean checkPassword(String password, String hash) {

    byte salt[];

    try {
      salt = saltSize > 0 ? Hex.decodeHex(hash.substring(0, saltSize * 2).toCharArray()) : null;
    } catch (DecoderException e) {
      throw new RuntimeException(e);
    }

    return hashPassword(salt, password).equals(hash);

  }

  private String hashPassword(byte[] salt, String password) {

    byte digest[];

    if(salt != null) {
      messageDigest.update(salt);
    }

    try {
      digest = messageDigest.digest(password.getBytes(passwordEncoding));
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }

    String hashString = Hex.encodeHexString(digest).substring(0, hashStringSize);

    if(salt != null) {
      return Hex.encodeHexString(salt) + hashAlg;
    } else {
      return hashString;
    }

  }

}

package biz.gelicon.sjua.model;

import javax.persistence.*;

@Entity(name = "Attribute")
@Table(name = "ATTRIBUTE")
@SequenceGenerator(name="AttributeIdGen", sequenceName = "attribute_id_gen")
public class Attribute {

    private Integer id;

    private AttributeType type;

    private String name;

    private AttributeRef attributeRef;

    @Id
    @GeneratedValue(generator="AttributeIdGen")
    @Column(name = "attribute_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "attributetype_id")
    public AttributeType getType() {
        return type;
    }

    public void setType(AttributeType type) {
        this.type = type;
    }

    @Column(name = "ATTRIBUTE_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

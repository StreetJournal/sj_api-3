package org.streetjournal.mobileapi.xml.xmlrequest.object;

import org.streetjournal.mobileapi.xml.AbstractRequest;
import org.streetjournal.mobileapi.xml.IssueSortOrderAdapter;
import org.streetjournal.mobileapi.xml.SortOrder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

/**
 * Зарос списка идентификаторов проблем, подпадающих под указанные критерии и отсортированных в
 * указанном порядке.
 */
@XmlRootElement(name = "Request")
public class ObjectListRequest extends AbstractRequest {

    private Integer limit;

    private Boolean useSerial;

    private String region;

    private List<String> categories;

    private Integer layerType;

    private SortOrder order;

    @XmlElement(name = "Limit")
    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @XmlElement(name = "UseSerial")
    public Boolean getUseSerial() {
        return useSerial;
    }

    public void setUseSerial(Boolean useSerial) {
        this.useSerial = useSerial;
    }

    @XmlElement(name = "Region")
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @XmlElement(name = "Category")
    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @XmlElement(name = "Order")
    @XmlJavaTypeAdapter(IssueSortOrderAdapter.class)
    public SortOrder getOrder() {
        return order;
    }

    public void setOrder(SortOrder order) {
        this.order = order;
    }

    @XmlElement(name = "LayerType")
    public Integer getLayerType() {
        return layerType;
    }

    public void setLayerType(Integer layerType) {
        this.layerType = layerType;
    }
}

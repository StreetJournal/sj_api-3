package org.streetjournal.mobileapi.api.system;

import biz.gelicon.server.service.PasswordHash;
import biz.gelicon.server.service.TicketControl;
import biz.gelicon.sjua.model.UserModel;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.streetjournal.mobileapi.config.Configuration;
import org.streetjournal.mobileapi.xml.AbstractResponse;
import org.streetjournal.mobileapi.xml.xmlrequest.system.SignupRequest;
import org.streetjournal.mobileapi.xml.xmlresponse.system.LoginErrorResponse;
import org.streetjournal.mobileapi.xml.xmlresponse.system.LoginResponse;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 */
@javax.ws.rs.Path("signup")
public class SignupHandler {

    private static final Logger logger = LoggerFactory.getLogger(SignupHandler.class);

    @Inject
    EntityManager em;
    @Inject
    Configuration configuration;

    @Inject
    TicketControl ticketControl;

    @Inject
    PasswordHash passwordHash;

    private SignupRequest request;

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public AbstractResponse execute(SignupRequest request) {

        this.request = request;

        UserModel user = checkLogin(request.getEmail(), request.getPassword());

        if (user != null) {
            ticketControl.setTicket(System.currentTimeMillis(), user.getId());
            return new LoginResponse(ticketControl.getHexTicket(), user.getId().longValue(), formatName(user), "validated", 1);
        }

        Client c = Client.create();
        WebResource r = c.resource(getRegistrationUrl());

        JSONObject requestData = new JSONObject();

        try {
            requestData
                    .put("applyterms", true)
                    .put("email", request.getEmail())
                    .put("lastname", request.getLastName())
                    .put("name", request.getFirstName())
                    .put("password", request.getPassword())
                    .put("password2", request.getPassword())
                    .put("toponym_id", getAreaId()); // UA
//                    .put("toponym_name", "Автономная Республика Крым");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        String bean = r
                .entity(requestData)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .post(String.class);

        JSONObject resultBean;

        try {
            resultBean = new JSONObject(bean);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        boolean success;

        try {
            success = resultBean.getBoolean("success");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        logger.info("Result=[{}]", bean.toString());

        if (!success) {
            return new LoginErrorResponse("bad_login", "Bad login or password");
        }

//        if (bean != null) {
//            List<GeoRegion> regions = bean.getRegions();
//            if (regions != null && regions.size() > 0) {
//                String result = regions.get(0).getCode();
//                logger.info("Geocode to [{}]", result);
//                return result;
//            }
//        }


        user = findLogin(request.getEmail());

        ticketControl.setTicket(System.currentTimeMillis(), user.getId());
        return new LoginResponse(ticketControl.getHexTicket(), user.getId().longValue(), formatName(user), "ok", 0);

    }

    private int getAreaId() {
        if (configuration.database.url.toLowerCase().contains("streetjournal.org")) {
            return 15816;
        }
        return 33964;
    }

    private String getRegistrationUrl() {
        if (configuration.database.url.toLowerCase().contains("streetjournal.org")) {
            return "http://www.streetjournal.org/register_json";
        }
        return "http://opencity.in.ua/register_json";
    }

    private UserModel checkLogin(String login, String password) {

        UserModel user;

        try {

            user =
                    em.createQuery("SELECT e FROM User e WHERE LOWER(e.email)=:name", UserModel.class)
                            .setParameter(
                                    "name", login.toLowerCase()).getSingleResult();

        } catch (NoResultException e) {
            return null;
        }

        if (!passwordHash.checkPassword(password, user.getPassword())) {
            logger.info(String.format("Bad password for user=[%s]", login));
            return null;
        }

        return user;

    }

    private UserModel findLogin(String login) {

        UserModel user;

        try {

            user =
                    em.createQuery("SELECT e.user FROM UserCommunication e WHERE LOWER(e.value)=:name", UserModel.class)
                            .setParameter(
                                    "name", login.toLowerCase()).getSingleResult();

        } catch (NoResultException e) {
            return null;
        }

        return user;
/*Firebird - @localhost:3051
        UserModel user;

        try {

            user =
                    em.createQuery("SELECT e FROM User e WHERE LOWER(e.email)=:name", UserModel.class)
                            .setParameter(
                                    "name", login.toLowerCase()).getSingleResult();

        } catch (NoResultException e) {
            return null;
        }

        return user;
*/
    }

    private static String formatName(UserModel user) {

        StringBuilder sb = new StringBuilder();

        appendWord(sb, user.getName());
        appendWord(sb, user.getSurname());

        return sb.toString();

    }

    private static void appendWord(StringBuilder stringBuilder, String value) {

        if (value != null && value.length() > 0) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(' ');
            }
            stringBuilder.append(value);
        }

    }

}
package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Локализованное имя топонима
 */
@Entity(name = "ToponymName")
@Table(name = "toponymlocale")
@SequenceGenerator(name = "ToponymNameIdGen", sequenceName = "toponymlocale_id_gen")
public class ToponymNameModel {

  private Integer id;

  private String name;

  private MessageLocaleModel locale;

  private ToponymModel toponym;

  @Id
  @GeneratedValue(generator = "ToponymNameIdGen")
  @Column(name = "toponymlocale_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "toponymlocale_name", nullable = false, length = 200)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @ManyToOne
  @JoinColumn(name = "messagelocale_id", nullable = false)
  public MessageLocaleModel getLocale() {
    return locale;
  }

  public void setLocale(MessageLocaleModel locale) {
    this.locale = locale;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "toponym_id", nullable = false)
  public ToponymModel getToponym() {
    return toponym;
  }

  public void setToponym(ToponymModel toponym) {
    this.toponym = toponym;
  }

}

package org.streetjournal.mobileapi.service.geo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class GeoRegions {

    private List<GeoRegion> regions;

    @XmlElement(name = "regions")
    public List<GeoRegion> getRegions() {
        return regions;
    }

    public void setRegions(List<GeoRegion> regions) {
        this.regions = regions;
    }

}

package org.streetjournal.mobileapi.xml.xmlresponse.object;

import org.streetjournal.mobileapi.xml.AbstractResponse;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 */
@XmlRootElement(name = "Response")
public class ObjectEventsResponse extends AbstractResponse {

    private List<ObjectCommentData> objects;

    public ObjectEventsResponse() {
    }

    public ObjectEventsResponse(List<ObjectCommentData> objects/*, List<IssueDispatchData> dispatches*/) {
        super(0, "Events found");
        this.objects = objects;
    }

    @XmlElementRef
    public List<ObjectCommentData> getObjects() {
        return objects;
    }

}

package org.streetjournal.mobileapi.xml;

/**
 *
 */
public class ModerationStatusAdapter extends AbstractEnumAdapter<ModerationStatus>  {

    public ModerationStatusAdapter() {
        super(ModerationStatus.class);
    }

}

package org.streetjournal.mobileapi.service;

import biz.gelicon.sjua.model.IssueCommentModel;
import biz.gelicon.sjua.model.IssueModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.Date;

/**
 *
 */
public class IssueHelper {

    private static final Logger logger = LoggerFactory.getLogger(IssueHelper.class);

    @Inject
    EntityManager em;

    public IssueCommentModel startUpdate(IssueModel issue) {

        IssueCommentModel rootComment = findRootComment(issue);

        Date now = new Date();

        em.createQuery("UPDATE IssueComment e SET e.modificationDate=:now WHERE e=:root")
            .setParameter("now", now)
            .setParameter("root", rootComment)
            .executeUpdate();

        logger.info("Updated {} to {}", rootComment.getId(), now);

        return rootComment;
    }

    public IssueCommentModel findRootCommentForUpdate(IssueModel issue) {

//        IssueCommentModel rootComment = findRootComment(issue);
//
//        Date now = new Date();
//
//        int changed = em.createQuery("UPDATE IssueComment e SET e.modificationDate=:now WHERE e=:root")
//            .setParameter("now", now)
//            .setParameter("root", rootComment)
//            .executeUpdate();
//
//        logger.info("Updated {} to {}", rootComment.getId(), now);

        return null;

    }

    public IssueCommentModel findRootComment(IssueModel issue) {

        Integer issueCommentId = em.createQuery(
            "SELECT e.id FROM IssueComment e " +
                "WHERE e.issue=:issue AND e.replyTo IS NULL", Integer.class
        )
            .setParameter("issue", issue)
            .getSingleResult();

        return em.getReference(IssueCommentModel.class, issueCommentId);
    }

//    public void updateModificationDate(IssueCommentModel issueComment) {
//        if(em.)
//    }
//
//    public void setUpdateTimeIssueComment(IssueCommentModel issueComment) {
//
//        Date now = new Date();
//
//        if (issueComment.getCreationDate() == null) {
//            issueComment.setCreationDate(now);
//        }
//
//        issueComment.setModificationDate(now);
//    }

}

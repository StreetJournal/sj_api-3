package biz.gelicon.sjua.gwasmodel;

import javax.persistence.*;

/**
 *
 */
@Entity(name = "Constant")
@Table(name = "constant")
public class ConstantModel {

  private Integer id;

  private CapResourceModel resource;

  @Id
  @Column(name = "constant_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @OneToOne
  @PrimaryKeyJoinColumn
  public CapResourceModel getResource() {
    return resource;
  }

  public void setResource(CapResourceModel resource) {
    this.resource = resource;
  }

}

package biz.gelicon.sjua.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 */
@Entity(name = "PointIcon")
@Table(name = "pointimage")
@SequenceGenerator(name="PointIconIdGen", sequenceName = "pointimage_id_gen")
public class PointIconModel {

  private Integer id;

  private LayerModel layer;

  private IssueStatusModel issueStatus;

  private Integer number;

  private Integer defaultValue;

  private String position;

  @Id
  @GeneratedValue(generator="PointIconIdGen")
  @Column(name = "pointimage_id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @ManyToOne
  @JoinColumn(name = "layer_id", nullable = false)
  public LayerModel getLayer() {
    return layer;
  }

  public void setLayer(LayerModel layer) {
    this.layer = layer;
  }

  @ManyToOne
  @JoinColumn(name="issuestatus_id", nullable = false)
  public IssueStatusModel getIssueStatus() {
    return issueStatus;
  }

  public void setIssueStatus(IssueStatusModel issueStatus) {
    this.issueStatus = issueStatus;
  }

  @Column(name="pointimage_number", nullable = false)
  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }

  @Column(name="pointimage_default", nullable = false)
  public Integer getDefaultValue() {
    return defaultValue;
  }

  public void setDefaultValue(Integer defaultValue) {
    this.defaultValue = defaultValue;
  }

  @Column(name="pointimage_position", nullable = true)
  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

}
